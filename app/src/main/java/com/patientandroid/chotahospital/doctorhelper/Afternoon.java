package com.patientandroid.chotahospital.doctorhelper;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Afternoon extends Fragment {

    View inflatedView = null;
    String date;
    ListView listView;
    Adapter_appointment adapterAppointment;


    public Afternoon() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.inflatedView = inflater.inflate(R.layout.fragment_afternoon, container, false);
        date = getArguments().getString("Date");

        listView = (ListView) inflatedView.findViewById(R.id.list_view);
        adapterAppointment = new Adapter_appointment(getActivity(), R.layout.appointment_row);
        listView.setAdapter(adapterAppointment);

        db_handler database = new db_handler(getActivity());/**Initialised the database object*/
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        Cursor cursor = database.getinfo_appointment(sqLiteDatabase,date,12,17);/**return information from database*/
        /**Analyzing the cursor object*/
        if (cursor.moveToFirst()) {

            do {
                /**Providing information of each row to DataProvider*/
                pat_appointment appointment = new pat_appointment();
                appointment.pat_mob = cursor.getString(1);
                appointment.pat_name = cursor.getString(2);
                appointment.pat_gender = cursor.getString(4);
                appointment.pat_age  = cursor.getString(3);
                appointment.pat_appointment_hour = cursor.getInt(5);
                appointment.pat_appointment_minute = cursor.getInt(6);
                /**Provide each of these to adapter for dispalying*/
                adapterAppointment.add(appointment);
            } while (cursor.moveToNext());
            database.close();
        }

        Button button = (Button) inflatedView.findViewById(R.id.p_add);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), add_schedule.class);
                intent.putExtra("Date",date);
                intent.putExtra("Fragment_type" , 1);
                startActivity(intent);
            }
        });

        return  inflatedView;
    }

}
