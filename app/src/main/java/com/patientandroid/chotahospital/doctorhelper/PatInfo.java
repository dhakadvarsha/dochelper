package com.patientandroid.chotahospital.doctorhelper;

/**
 * Created by Lalit Gautam on 6/1/2016.
 */
public class PatInfo
{
    public String p_contact_no;
    public String p_name;
    public String p_email_id;
    public String p_age;
    public String p_gender;
}
