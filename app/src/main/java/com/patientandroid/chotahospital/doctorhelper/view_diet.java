package com.patientandroid.chotahospital.doctorhelper;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class view_diet extends AppCompatActivity {

    TextView date;
    ViewPager viewPager1,viewPager2;
    ViewPagerAdapter viewPagerAdapter1 , viewPagerAdapter2;
    Firebase root_ref_diet;
    private static final String REFERENCE_URL_DIET = "https://chotahospital-ffc48.firebaseio.com/Diet";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_diet);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Diet Chart");

        Firebase.setAndroidContext(this);//Initialises firebase for using this android app
        root_ref_diet = new Firebase(REFERENCE_URL_DIET);//Reference to data into firebase cloud.

        date = (TextView) findViewById(R.id.Date);
        final Calendar calendar = Calendar.getInstance();
        Integer month = calendar.get(Calendar.MONTH);
        Integer viewmonth = month + 1;
        String today = dateFormat(calendar.get(Calendar.DAY_OF_MONTH), month , viewmonth , calendar.get(Calendar.YEAR));
        date.setText(today);

        if(getIntent().getStringExtra("Date") != null){
            date.setText(getIntent().getStringExtra("Date"));
        }

        //Initially calls the callDiet and callCalorieview fragmaents with current date.
        CallDiet();
        CallCalorieView();

        /**
         * fragment for floating action button
         */
        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_layout);
        frameLayout.getBackground().setAlpha(0);

        final FloatingActionsMenu fabMenu = (FloatingActionsMenu) findViewById(R.id.fab_menu);
        fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                frameLayout.getBackground().setAlpha(240);
                frameLayout.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        fabMenu.collapse();
                        return true;
                    }
                });
            }

            @Override
            public void onMenuCollapsed() {
                frameLayout.getBackground().setAlpha(0);
                frameLayout.setOnTouchListener(null);
            }
        });

        FloatingActionButton breakfastButton = (FloatingActionButton) findViewById(R.id.fab_breakfast);
        breakfastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calladdFoodItem("Breakfast");
            }
        });

        FloatingActionButton lunchButton = (FloatingActionButton) findViewById(R.id.fab_lunch);
        lunchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calladdFoodItem("Lunch");
            }
        });

        FloatingActionButton dinnerButton = (FloatingActionButton) findViewById(R.id.fab_dinner);
        dinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calladdFoodItem("Dinner");
            }
        });
    }

    /**
     * called when the button left to date text is pressed.
     * Takes to the previous date of the corressponding displayed dates.
     * @param v
     */
    public void previous(View v){

        String current = date.getText().toString();
        String[] parts = current.split("-");

        Integer currentDayselected = Integer.parseInt(parts[0]);
        Integer viewMonth = Integer.parseInt(parts[1]);
        Integer currentMonthselected = Integer.parseInt(parts[1]) - 1;
        Integer currentYearselected = Integer.parseInt(parts[2]);

        Calendar cal = Calendar.getInstance();

        if(currentDayselected == 1){
            cal.set(Calendar.MONTH , currentMonthselected);
            if(currentMonthselected == 0){
                cal.set(Calendar.MONTH , 11);
                currentMonthselected = 11;
                viewMonth = 12;
                currentDayselected = getMaxDay(currentMonthselected,currentYearselected);
                currentYearselected--;
            }else{
                currentMonthselected--;
                --viewMonth;
                cal.set(Calendar.MONTH , currentMonthselected);
                currentDayselected = getMaxDay(currentMonthselected,currentYearselected);
            }
        }
        else{
            currentDayselected--;
        }

        Log.e("Date previous", String.valueOf(currentDayselected+"-"+currentMonthselected+"-"+currentYearselected));
        date.setText(dateFormat(currentDayselected,currentMonthselected,viewMonth,currentYearselected));

        CallDiet();
        CallCalorieView();
    }

    /**
     * called when the button right to date text is pressed.
     * Takes to the next date of the corressponding displayed dates.
     * @param v
     */
    public void next(View v){

        String current = date.getText().toString();
        String[] parts = current.split("-");

        Log.e("CuteentMonthselected", current);

        Integer currentDayselected = Integer.parseInt(parts[0]);
        Integer viewMonth = Integer.parseInt(parts[1]);
        Integer currentMonthselected = Integer.parseInt(parts[1])-1;
        Integer currentYearselected = Integer.parseInt(parts[2]);


        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH , currentMonthselected);

        Log.e("CuteentMonthselected", String.valueOf(currentMonthselected));
        Log.e("Month and maximun days",cal.get(Calendar.MONTH) + "-"+cal.getActualMaximum(Calendar.DAY_OF_MONTH));


        if(currentDayselected == getMaxDay(currentMonthselected,currentYearselected)){
            currentDayselected = 1;
            if(currentMonthselected == 11){
                currentMonthselected = 0;
                currentYearselected++;
                viewMonth = 1;
            }
            else{
                currentMonthselected++;
                ++viewMonth;
            }

        }
        else{
            currentDayselected++;
        }

        Log.e("Date next", String.valueOf(currentDayselected+"-"+currentMonthselected+"-"+currentYearselected));

        date.setText(dateFormat(currentDayselected,currentMonthselected,viewMonth,currentYearselected));

        CallDiet();
        CallCalorieView();

    }

    /**
     * Datepicker to pick the date from calendar
     */
    Calendar myCalendar2 = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar2.set(Calendar.YEAR, year);
            myCalendar2.set(Calendar.MONTH, monthOfYear);
            myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel2();
        }

    };

    public void DatePick(View view){
        String current = date.getText().toString();
        String[] parts = current.split("-");
        new DatePickerDialog(this, date2, Integer.parseInt(parts[2]),Integer.parseInt(parts[1]) - 1,Integer.parseInt(parts[0])).show();
    }

    private void updateLabel2() {

        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        date.setText(sdf.format(myCalendar2.getTime()));
        CallCalorieView();
        CallDiet();
    }

    /**
     *
     * @param currentMonthselected
     * @param currentYearselected
     * @return maximum number of days in an particular month
     */
    private Integer getMaxDay(Integer currentMonthselected , Integer currentYearselected){

        Calendar cal = Calendar.getInstance();
        Integer maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        if(currentMonthselected == 1){
            if((currentYearselected%4 == 0) || currentYearselected%100 == 0){
                maxDay = 29;
            }
            else{
                maxDay = 28;
            }
        }

        return maxDay;

    }

    /**
     *
     * @param currentDayselected
     * @param currentMonthselected
     * @param viewMonth
     * @param currentYearselected
     * @return date fromat in dd-mm-yyyy format
     */
    private String dateFormat(Integer currentDayselected ,Integer currentMonthselected ,Integer viewMonth ,
                              Integer currentYearselected){

        if(currentDayselected < 10){
            if(currentMonthselected < 10){
                return  "0"+currentDayselected+"-"+"0"+viewMonth+"-"+currentYearselected;
            }
            return "0"+currentDayselected+"-"+viewMonth+"-"+currentYearselected;
        }else if(currentMonthselected < 10){
            return  currentDayselected+"-"+"0"+viewMonth+"-"+currentYearselected;
        }else {
            return  currentDayselected+"-"+viewMonth+"-"+currentYearselected;
        }
    }

    /**
     * calls Calorieview fragment with date and patientid , to get total consumed calories on the specified date.
     */
    private void CallCalorieView(){

        calorieView calorieView = new calorieView();
        viewPager2 = (ViewPager) findViewById(R.id.viewpager_dietCalorie);

        viewPagerAdapter2 = new ViewPagerAdapter(getSupportFragmentManager());
        calorieView.SetPatientid(getIntent().getStringExtra("patid"));
        calorieView.SetDate(date.getText().toString());
        viewPagerAdapter2.add(calorieView, "");
        viewPagerAdapter2.notifyDataSetChanged();

        viewPager2.setOffscreenPageLimit(1);
        viewPager2.setAdapter(viewPagerAdapter2);

    }

    /**
     * calls the Diet fragment with a given patientid and date.
     */
    private void CallDiet(){

        Diet diet = new Diet();
        viewPager1 = (ViewPager) findViewById(R.id.viewpager_diet);

        viewPagerAdapter1 = new ViewPagerAdapter(getSupportFragmentManager());
        diet.SetPatientid(getIntent().getStringExtra("patid"));
        diet.SetDate(date.getText().toString());
        viewPagerAdapter1.add(diet, "");
        viewPagerAdapter1.notifyDataSetChanged();

        viewPager1.setOffscreenPageLimit(1);
        viewPager1.setAdapter(viewPagerAdapter1);

    }

    /**
     * takes to view_patient activity when the back button is pressed
     */
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this, view_patient.class);
        intent.putExtra("patid",getIntent().getStringExtra("patid"));
        intent.putExtra("Patient Name",getIntent().getStringExtra("Patient Name"));
        intent.putExtra("Position",getIntent().getIntExtra("Position",0));
        startActivity(intent);
        finish();

    }

    /**
     * calls eact time time food item is to be added
     * @param addTo : describes if the food item is to be added to breakfast , lunch or dinner.
     */
    private void calladdFoodItem(String addTo){

        Intent intent = new Intent(view_diet.this , addFoodItem.class);

        intent.putExtra("patid",getIntent().getStringExtra("patid"));
        intent.putExtra("Patient Name",getIntent().getStringExtra("Patient Name"));
        intent.putExtra("AddTo",addTo);
        intent.putExtra("Date",date.getText().toString());

        startActivity(intent);
        
    }


    /**
     * create option menu for the back button on action bar , to take to parent activity when thw home button is pressed
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_insulin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            Intent intent = new Intent(this, view_patient.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("Patient Name",getIntent().getStringExtra("Patient Name"));
            intent.putExtra("patid",getIntent().getStringExtra("patid"));
            startActivity(intent);
            this.finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

}