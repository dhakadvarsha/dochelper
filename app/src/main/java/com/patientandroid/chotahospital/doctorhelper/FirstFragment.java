package com.patientandroid.chotahospital.doctorhelper;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {

    private LineChart lineChart;
    ArrayList<Entry> x = new ArrayList<>();
    ArrayList<String> label = new ArrayList<>();
    ArrayList<Integer> color = new ArrayList<>();
    View inflatedView = null;
    String contactNo;

    Firebase root_ref;
    private static final String REFERENCE_URL = "https://chotahospital-ffc48.firebaseio.com/Patients";


    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.inflatedView = inflater.inflate(R.layout.fragment_first, container, false);

        Firebase.setAndroidContext(getContext());//Initialises firebase for using this android app
        root_ref = new Firebase(REFERENCE_URL);//Reference to data into firebase cloud.

        lineChart = new LineChart(getActivity());
        lineChart = (LineChart) inflatedView.findViewById(R.id.linechart);

        final Integer[] value = new Integer[1];
        final int[] val = {0};
        final String[] date = new String[1];


        Firebase patient = root_ref.child(contactNo);
        Firebase bloodGlucose = patient.child("BloodGlucose");
        bloodGlucose.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                x.clear();
                label.clear();
                color.clear();
                for (DataSnapshot dateData : dataSnapshot.getChildren()) {
                    Log.e("Date fragment",dateData.getKey());
                    for (DataSnapshot time : dateData.getChildren()) {
                        Glucose glucose = time.getValue(Glucose.class);
                        if(glucose.getBg_type().equals("Post-BreakFast")){

                            value[0] = Integer.valueOf(glucose.getBg_value());
                            date[0] = dateData.getKey();
                            //Log.e("Date",date);
                            label.add(date[0]);
                            x.add(new Entry(value[0], val[0]));
                            ++val[0];
                            color.add(Color.parseColor("#ff0000"));
                        }
                    }
                }
                Log.e("Run", String.valueOf(val[0]));
                LineDataSet dataset = new LineDataSet(x, "");
                LineData data = new LineData(label, dataset);
                lineChart.setData(data); // set the data and list of lables into chart

                dataset.setColor(Color.parseColor("#ffffff"));
                dataset.setCircleColor(Color.parseColor("#ffffff"));
                dataset.setCircleColors(color);
                dataset.setDrawCircleHole(false);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        //dataset.setDrawCubic(true);
        //dataset.setDrawFilled(true);
        //dataset.setColors(ColorTemplate.COLORFUL_COLORS);

//        Log.e("Run if loop", String.valueOf(val[0]));
//        if(val[0] == 0){
//            TextView textView = (TextView) inflatedView.findViewById(R.id.textView3);
//            textView.setText("No Data available");
//        }

        lineChart.setDescription("");
        lineChart.getXAxis().setDrawGridLines(false);
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lineChart.getAxisLeft().setDrawGridLines(false);
        lineChart.getAxisRight().setDrawGridLines(false);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.setBackgroundColor(Color.parseColor("#A901DB"));
        lineChart.getXAxis().setAvoidFirstLastClipping(true);
        lineChart.setNoDataTextDescription("");
        lineChart.invalidate();


        return inflatedView;
    }

    public void SetContact(String contactNumber){
        this.contactNo = contactNumber;
    }
}
