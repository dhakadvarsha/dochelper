package com.patientandroid.chotahospital.doctorhelper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by varsh on 15-06-2016.
 */
public class ViewDay extends FragmentStatePagerAdapter {

    ArrayList<Fragment> fragments = new ArrayList<>();

    //methods to initialize these array elemnts.
    public void add(Fragment fragment) {
        this.fragments.add(fragment);
    }

    public ViewDay(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
