package com.patientandroid.chotahospital.doctorhelper;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class add_insulin extends AppCompatActivity {

    EditText intake,start,end;
    String item2,contact_no;
    Context context = this;
    db_handler database;
    SQLiteDatabase sqLiteDatabase;
    Spinner spinner2;
    Insulin insulin = new Insulin();
    Cursor cursor;
    public Integer i=0,x=0,del = 0;
    Switch sr,sb,sl,se,sd,sn;

    Firebase root_ref;
    private static final String REFERENCE_URL = "https://chotahospital-ffc48.firebaseio.com/Patients";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_insulin);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("New Record");

        Firebase.setAndroidContext(this);//Initialises firebase for using this android app
        root_ref = new Firebase(REFERENCE_URL);//Reference to data into firebase cloud.

        contact_no = getIntent().getStringExtra("Contact_No");

        intake = (EditText) findViewById(R.id.Intake_value);
        start = (EditText) findViewById(R.id.start_date);
        end = (EditText) findViewById(R.id.end_date);

        spinner2 = (Spinner) findViewById(R.id.schedule2);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.insulin_type, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item2 = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        i = getIntent().getIntExtra("Value_id",0);

        final Integer temp = 0;
        final String[] DateSelected = {""};
        final String[] TypeBg = {""};
        if(getIntent().getStringExtra("DateUpdate") != null){

            Firebase contact = root_ref.child(contact_no);
            Firebase patient = contact.child("Insulin");
            Firebase onDateUpdate = patient.child(getIntent().getStringExtra("DateUpdate"));
            final String id = getIntent().getStringExtra("Positionbg");

            onDateUpdate.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for (DataSnapshot Reading : dataSnapshot.getChildren()) {
                        if(id != null){
                            if(Reading.getKey().equals(id)){

                                DateSelected[0] = Reading.getKey();
                                Log.e("Read is",Reading.getKey());
                                Insulin ReadData = Reading.getValue(Insulin.class);

                                intake.setText(ReadData.getI_intake_value());
                                start.setText(ReadData.getI_s_date());
                                end.setText(ReadData.getI_e_date());

                                spinner2 = (Spinner) findViewById(R.id.schedule2);
                                String[] schedule2 = {"Fast Acting", "Long Acting"};
                                int o = 0;
                                for (int j = 0; j < schedule2.length; j++) {
                                    if (ReadData.getI_type().equals(schedule2[j])) {
                                        o = j;
                                    }
                                }

                                spinner2.setSelection(o);

                                if(ReadData.getI_reminder() == 1){
                                    sr = (Switch) findViewById(R.id.switchr);
                                    sr.setChecked(true);

                                }

                                if(ReadData.getI_breakfast() == 1){
                                    sb = (Switch) findViewById(R.id.switchb);
                                    sb.setChecked(true);

                                }

                                if(ReadData.getI_lunch() == 1){
                                    sl = (Switch) findViewById(R.id.switchl);
                                    sl.setChecked(true);

                                }

                                if(ReadData.getI_snacks() == 1){
                                    se = (Switch) findViewById(R.id.switche);
                                    se.setChecked(true);

                                }

                                if(ReadData.getI_dinner() == 1){
                                    sd = (Switch) findViewById(R.id.switchd);
                                    sd.setChecked(true);

                                }

                                if(ReadData.getI_night() == 1){
                                    sn = (Switch) findViewById(R.id.switchn);
                                    sn.setChecked(true);

                                }

                            }
                        }
                        if(!(DateSelected[0].equals(""))){
                            break;
                        }
                    }


                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }
    }

    Calendar myCalendar1 = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar1.set(Calendar.YEAR, year);
            myCalendar1.set(Calendar.MONTH, monthOfYear);
            myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel1();
        }

    };

    public void Datepicker1(View view){
        new DatePickerDialog(this, date1, myCalendar1.get(Calendar.YEAR), myCalendar1.get(Calendar.MONTH), myCalendar1.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLabel1() {

        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        start.setText(sdf.format(myCalendar1.getTime()));
    }

    Calendar myCalendar2 = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar2.set(Calendar.YEAR, year);
            myCalendar2.set(Calendar.MONTH, monthOfYear);
            myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel2();
        }

    };

    public void Datepicker2(View view){
        new DatePickerDialog(this, date2, myCalendar2.get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH), myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLabel2() {

        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        if((myCalendar1.getTime()).before((myCalendar2.getTime()))){
            end.setText(sdf.format(myCalendar2.getTime()));
        }
        else{
            Toast.makeText(this , "Please enter a valid end date" , Toast.LENGTH_LONG).show();
        }
    }

    public void add_intake(View view){

        insulin.i_intake_value = intake.getText().toString();
        insulin.i_s_date = start.getText().toString();
        insulin.i_e_date = end.getText().toString();
        insulin.i_type = item2;

        Switch reminder = (Switch) findViewById(R.id.switchr);
        reminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });
        insulin.i_reminder = 0;
        if (reminder.isChecked()) {
            insulin.i_reminder = 1;
        }

        Switch breakfast = (Switch) findViewById(R.id.switchb);
        breakfast.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });

        insulin.i_breakfast = 0;
        if (breakfast.isChecked()) {
            insulin.i_breakfast = 1;
            ++x;
        }

        Switch lunch = (Switch) findViewById(R.id.switchl);
        lunch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });

        insulin.i_lunch = 0;
        if (lunch.isChecked()) {
            insulin.i_lunch = 1;
            ++x;
        }

        Switch evening = (Switch) findViewById(R.id.switche);
        evening.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });
        insulin.i_snacks = 0;
        if (evening.isChecked()) {
            insulin.i_snacks = 1;
            ++x;
        }

        Switch dinner = (Switch) findViewById(R.id.switchd);
        dinner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });
        insulin.i_dinner = 0;
        if (dinner.isChecked()) {
            insulin.i_dinner = 1;
            ++x;
        }

        Switch night = (Switch) findViewById(R.id.switchn);
        night.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });
        insulin.i_night = 0;
        if (night.isChecked()) {
            insulin.i_night = 1;
            ++x;
        }

        if((intake.getText().toString()).equals("")){
            Toast.makeText(this , "Enter the Intake value",Toast.LENGTH_LONG).show();
        }
        else if((start.getText().toString()).equals(""))
        {
            Toast.makeText(this , "Enter the Start date",Toast.LENGTH_LONG).show();
        }

        else if((end.getText().toString()).equals("")){
            Toast.makeText(this , "Enter the End date",Toast.LENGTH_LONG).show();
        }

        else if(x == 0){

            AlertDialog.Builder builder = new AlertDialog.Builder(add_insulin.this);
            builder.setCancelable(true);
            builder.setTitle("You haven't selected any timings.Do you still want to add?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    function();
                }

            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                }

            });
            AlertDialog alert = builder.create();
            alert.show();

        }

        else{
            function();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_insulin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        /** if (id == R.id.editprofile) {
         Toast.makeText(this , "About to call Profile.class" , Toast.LENGTH_LONG).show();
         Intent intent = new Intent(add_insulin.this, Profile.class);
         intent.putExtra("Profile Update", 1);
         startActivity(intent);
         this.finish();
         }

         */

        if(id == android.R.id.home) {
            Intent intent = new Intent(this, view_insulin.class);
            intent.putExtra("Contact_No",getIntent().getStringExtra("Contact_No"));
            intent.putExtra("Position",getIntent().getIntExtra("Position",0));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this, view_insulin.class);
        intent.putExtra("Contact_No",contact_no);
        intent.putExtra("Position",getIntent().getIntExtra("Position",0));
        startActivity(intent);
        finish();

    }

    public void function(){
        if(i==1){

            Firebase patient = root_ref.child(contact_no);
            Firebase bgData = patient.child("Insulin");
            Firebase DatePrevious = bgData.child(getIntent().getStringExtra("DateUpdate"));
            Firebase DateData = bgData.child(start.getText().toString());
            Firebase idRef = DateData.child(getIntent().getStringExtra("Positionbg"));

            if(!((start.getText().toString()).equals(getIntent().getStringExtra("DateUpdate")))){
                DatePrevious.child(getIntent().getStringExtra("Positionbg")).removeValue();
                Log.e("Remove",getIntent().getStringExtra("Positionbg"));
            }
            idRef.setValue(insulin);

            Toast.makeText(getBaseContext(),"Data modified",Toast.LENGTH_LONG).show();

            Intent intent = new Intent(add_insulin.this , view_insulin.class);
            intent.putExtra("Contact_No",contact_no);
            intent.putExtra("Position",getIntent().getIntExtra("Position",0));
            startActivity(intent);
            add_insulin.this.finish();
        }

        else{

            Firebase patient = root_ref.child(contact_no);
            Firebase bgData = patient.child("Insulin");
            Firebase DateData = bgData.child(insulin.i_s_date);

            DateData.push().setValue(insulin);

            Toast.makeText(getBaseContext(),"Data saved",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(add_insulin.this , view_insulin.class);
            intent.putExtra("Contact_No",contact_no);
            intent.putExtra("Position",getIntent().getIntExtra("Position",0));
            startActivity(intent);
            add_insulin.this.finish();
        }
    }
}
