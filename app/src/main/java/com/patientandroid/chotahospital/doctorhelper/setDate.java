package com.patientandroid.chotahospital.doctorhelper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.util.Calendar;

public class setDate extends AppCompatActivity {

    EditText day,month,year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_date);

        day = (EditText) findViewById(R.id.day);
        month = (EditText) findViewById(R.id.month);
        year = (EditText) findViewById(R.id.year);
    }

    public void go(View view){
        Intent intent = new Intent(this , appointmentSchedule.class);
        intent.putExtra("Go",1);
        intent.putExtra("Day",day.getText().toString());
        intent.putExtra("Month",month.getText().toString());
        intent.putExtra("Year",year.getText().toString());

        startActivity(intent);
        this.finish();
    }

    public void today(View view){
        Calendar calendar = Calendar.getInstance();
        Intent intent = new Intent(this,appointmentSchedule.class);
        intent.putExtra("Go",0);
        intent.putExtra("Day",String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        intent.putExtra("Month",String.valueOf(calendar.get(Calendar.MONTH)));
        intent.putExtra("Year",String.valueOf(calendar.get(Calendar.YEAR)));

        startActivity(intent);
        this.finish();
    }
}
