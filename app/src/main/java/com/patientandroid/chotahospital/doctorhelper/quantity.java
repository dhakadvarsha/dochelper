package com.patientandroid.chotahospital.doctorhelper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;

import java.util.Calendar;

public class quantity extends AppCompatActivity {

    EditText quantity;
    Spinner spinner2;
    TextView foodItemName,calories;
    String item2;

    Firebase root_ref_diet;
    private static final String REFERENCE_URL_DIET = "https://chotahospital-ffc48.firebaseio.com/Diet";

    //to make the activity popup
    public void makeItAPopUp(){
        DisplayMetrics dm=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int wi=dm.widthPixels;
        int h=dm.heightPixels;
        getWindow().setLayout((int)(wi*.8),(int)(h*.8));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quantity);
        makeItAPopUp();

        Firebase.setAndroidContext(this);//Initialises firebase for using this android app
        root_ref_diet = new Firebase(REFERENCE_URL_DIET);//Reference to data into firebase cloud.

        foodItemName = (TextView) findViewById(R.id.foodName);
        calories = (TextView) findViewById(R.id.Calories);
        quantity = (EditText) findViewById(R.id.quantity);
        spinner2 = (Spinner) findViewById(R.id.quantityList);

        foodItemName.setText(getIntent().getStringExtra("FoodItem"));
        calories.setText(getIntent().getStringExtra("CaloriesPerGram") + "/100gm");

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.quantityType, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item2 = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if(getIntent().getIntExtra("update",0) == 1){
            int k = 0;
            String quantityPassUpdate = getIntent().getStringExtra("quantityPassUpdate");
            String[] parts = quantityPassUpdate.split(" ");
            quantity.setText(parts[0]);

            String [] type = {"gram", "plate", "ml", "cup", "bowl"};
            for(int j=0;j<type.length;j++){
                if (parts[1].equals(type[j])){
                    k=j;
                }
            }
            spinner2.setSelection(k);

            Integer cal = Integer.parseInt(getIntent().getStringExtra("CaloriesPerGram"));
            String[] part = quantityPassUpdate.split(" ");
            Double quantity = Double.parseDouble(part[0]);
            calories.setText(String.valueOf((quantity*cal)/100));
            
        }
    }

    /**
     * called when the saves button is preesed.
     * @param view
     */
    public void save(View view){

        storeInfo store = new storeInfo();
        store.itemId = getIntent().getStringExtra("foodid");
        store.quantity = quantity.getText().toString()+ " " + item2;

        if(item2.equals("gram") && !(quantity.getText().toString()).equals("")){
            Integer cal = Integer.parseInt(getIntent().getStringExtra("CaloriesPerGram"));
            Log.e("Calories/100gm", String.valueOf(cal));
            calories.setText(String.valueOf((cal*Double.parseDouble(quantity.getText().toString()))/100) + " calories");
            Log.e("Calories overall", String.valueOf(cal));

            store.caloriesConsumed = String.valueOf((cal*Double.parseDouble(quantity.getText().toString()))/100);
            Firebase patient = root_ref_diet.child(getIntent().getStringExtra("patid"));
            Firebase Date = patient.child(getIntent().getStringExtra("Date"));
            Firebase breakfast = Date.child(getIntent().getStringExtra("AddTo"));

            if(getIntent().getIntExtra("update",0) == 1){

                breakfast.child(getIntent().getStringExtra("updateid")).setValue(store);
                Toast.makeText(this , "Data Modified",Toast.LENGTH_LONG).show();
            }
            else{

                breakfast.push().setValue(store);
                Toast.makeText(this , "New data entered",Toast.LENGTH_LONG).show();
            }

            Intent intent = new Intent(this,addFoodItem.class);

            intent.putExtra("patid",getIntent().getStringExtra("patid"));
            intent.putExtra("Patient Name",getIntent().getStringExtra("Patient Name"));
            intent.putExtra("Date",getIntent().getStringExtra("Date"));
            intent.putExtra("AddTo",getIntent().getStringExtra("AddTo"));

            startActivity(intent);
            this.finish();

        }
        else{
            Toast.makeText(this , "Quantity not entered" , Toast.LENGTH_LONG).show();
        }

    }
}
