package com.patientandroid.chotahospital.doctorhelper;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class Diet extends Fragment {

    String patientid,date,calories;
    View inflatedview;
    int breakfastCount = 0 , lunchCount = 0 , dinnerCount = 0;

    Firebase root_ref_diet;
    private static final String REFERENCE_URL_DIET = "https://chotahospital-ffc48.firebaseio.com/Diet";

    Map<String,String> mapFoodIdToKey = new HashMap<>();
    Map<String,String> mapFoodIdToName = new HashMap<>();
    Map<String,String> mapFoodIdToQuantity = new HashMap<>();
    Map<String,String> mapIdToCalories = new HashMap<>();


    public Diet() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        inflatedview = inflater.inflate(R.layout.fragment_diet, container, false);

        Firebase.setAndroidContext(getContext());//Initialises firebase for using this android app
        root_ref_diet = new Firebase(REFERENCE_URL_DIET);//Reference to data into firebase cloud.


        FirebaseDatabase database = FirebaseDatabaseUtil.getDatabase();

        //adding food item list for breakfast , lunch and dinner
        DatabaseReference Date = database.getReference("Diet/" + patientid +"/"+date);
        Date.addValueEventListener(new com.google.firebase.database.ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {

                mapIdToCalories.clear();
                mapFoodIdToQuantity.clear();
                mapFoodIdToName.clear();
                mapFoodIdToKey.clear();

                if(dataSnapshot.exists()){

                    for (com.google.firebase.database.DataSnapshot key : dataSnapshot.getChildren()) {

                        for (com.google.firebase.database.DataSnapshot Reading : key.getChildren()) {
                            Log.e("Reading is",Reading.getKey());

                            storeInfo storeinfo = Reading.getValue(storeInfo.class);
                            String id = storeinfo.getItemId();
                            mapFoodIdToKey.put(id ,Reading.getKey());
                            //mapping food id with the key name

                            try {

                                JSONObject jsonObject = getJsonObject(id);

                                Food food= new Food();
                                food.fooditem = jsonObject.getString("FIELD2");
                                food.foodquantity = storeinfo.getQuantity();

                                mapFoodIdToName.put(id , food.fooditem);//mapping food id with food name
                                mapFoodIdToQuantity.put(id , food.foodquantity);//mapping food id with consumed quantity
                                mapIdToCalories.put(id ,jsonObject.getString("FIELD4"));//mapping food id to calories per 100gm

                                Log.e("Passing value ",food.fooditem + " , " + id + " , " + Reading.getKey() + " , " +
                                        food.foodquantity + " gram"  + " , " + jsonObject.getString("FIELD4"));

                                if(!(jsonObject.getString("FIELD8").equals("-"))){
                                    food.carbohydrates = "carbohydrates : "+jsonObject.getString("FIELD8")+" gm";
                                }else{
                                    food.carbohydrates = "carbohydrates : "+jsonObject.getString("FIELD8");
                                }
                                if(!(jsonObject.getString("FIELD10").equals("-"))){
                                    food.protein = "protein : "+jsonObject.getString("FIELD10")+" gm";
                                }else{
                                    food.protein = "protein : "+jsonObject.getString("FIELD10");
                                }

                                //get the Linear layout to with the text view should be added
                                final LinearLayout linearLayout;
                                if(key.getKey().equals("Breakfast")){
                                    linearLayout = (LinearLayout) inflatedview.findViewById(R.id.linearLayoutBreakfast);
                                    linearLayout.setTag(1);//setting tag to linera layout
                                    if(breakfastCount == 0){
                                        linearLayout.removeAllViews();
                                        breakfastCount = 1;//keeping the count to check if altleast one item is contained in the meal.
                                    }
                                }
                                else if(key.getKey().equals("Lunch")){
                                    linearLayout = (LinearLayout) inflatedview.findViewById(R.id.linearLayoutLunch);
                                    linearLayout.setTag(2);//setting tag to linera layout
                                    if(lunchCount == 0){
                                        linearLayout.removeAllViews();
                                        lunchCount = 1;//keeping the count to check if altleast one item is contained in the meal.
                                    }
                                }
                                else{
                                    linearLayout = (LinearLayout) inflatedview.findViewById(R.id.linearLayoutDinner);
                                    linearLayout.setTag(3);//setting tag to linera layout
                                    if(dinnerCount == 0){
                                        linearLayout.removeAllViews();
                                        dinnerCount = 1;//keeping the count to check if altleast one item is contained in the meal.

                                    }
                                }

                                TextView textView1 = new TextView(getContext());
                                textView1.setId(Integer.parseInt(id));
                                /**
                                 * get the text view id to be modified and also it's linear layout.
                                 */
                                textView1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        String layout_name;

                                        //Log.e("LayoutName", layout_name.toString());
                                        switch ((Integer) ((ViewGroup)v.getParent()).getTag()) {
                                            case 1:
                                                layout_name = "Breakfast";
                                                break;
                                            case 3:
                                                layout_name = "Dinner";
                                                break;
                                            default:
                                                layout_name = "Lunch";
                                                break;
                                        }
                                        modify(v.getId() , layout_name);
                                    }
                                });

                                /**
                                * get the text view id to be removed and also it's linear layout.
                                 */
                                textView1.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {

                                        String layout_name;
                                        switch ((Integer) ((ViewGroup)v.getParent()).getTag()) {
                                            case 1:
                                                layout_name = "Breakfast";
                                                break;
                                            case 3:
                                                layout_name = "Dinner";
                                                break;
                                            default:
                                                layout_name = "Lunch";
                                                break;
                                        }

                                        remove(v.getId() , layout_name);
                                        return false;
                                    }
                                });

                                textView1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT));
                                textView1.setText(jsonObject.getString("FIELD2")+ "  " +food.foodquantity + "\n" + food.carbohydrates + "\n" + food.protein );
                                textView1.setPadding(20, 20, 20, 20);// in pixels (left, top, right, bottom)
                                linearLayout.addView(textView1);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return  inflatedview;
    }

    /**
     * called when any text view is clicked and is then redirected to quantity class to update the information.
     * @param id1 : food item's id
     * @param addto : wher the item is to be modified
     */
    public void modify(Integer id1 , String addto){

        String id = String.valueOf(id1);
        Intent intent = new Intent(getActivity(),quantity.class);

        intent.putExtra("FoodItem",mapFoodIdToName.get(id));
        intent.putExtra("patid",patientid);
        intent.putExtra("Date",date);
        intent.putExtra("foodid",id);
        intent.putExtra("update",1);
        intent.putExtra("AddTo",addto);
        intent.putExtra("updateid",mapFoodIdToKey.get(id));
        intent.putExtra("quantityPassUpdate",mapFoodIdToQuantity.get(id));
        intent.putExtra("CaloriesPerGram",mapIdToCalories.get(id));
        startActivity(intent);

    }

    /**
     * Alert message is fired to confirm the delete action.
     * @param id1 : food item's id
     * @param deletefrom : if name in the database from where the data is removed.
     */
    public void remove(final Integer id1 , final String deletefrom){

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setTitle("Do you want to delete "+mapFoodIdToName.get(String.valueOf(id1))+"?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                Firebase patient = root_ref_diet.child(patientid);
                Firebase Date = patient.child(date);
                Firebase time = Date.child(deletefrom);
                Log.e("Delete from ",deletefrom);
                time.child(mapFoodIdToKey.get(String.valueOf(id1))).removeValue();

                LinearLayout linearLayout1 , linearLayout2 ,linearLayout3;
                linearLayout1 = (LinearLayout) inflatedview.findViewById(R.id.linearLayoutBreakfast);
                linearLayout1.removeAllViews();
                linearLayout2 = (LinearLayout) inflatedview.findViewById(R.id.linearLayoutLunch);
                linearLayout2.removeAllViews();
                linearLayout3 = (LinearLayout) inflatedview.findViewById(R.id.linearLayoutDinner);
                linearLayout3.removeAllViews();
            }

        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

            }

        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    //sets the person's id whose calories are to be calculated
    public void SetPatientid(String personid){
        this.patientid = personid;
    }

    //set Date for which total calories are calculated
    public void SetDate(String date){
        this.date = date;
    }


    /**
     * reads the json file form assets
     * @return string with json format
     */
    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getActivity().getAssets().open("convertcsv.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /**
     * returen jsonobject for particular id from json array
     * @param id
     * @return jsonobject with FIELD1 = id
     * @throws JSONException
     */
    public JSONObject getJsonObject(String id) throws JSONException {

        JSONArray jsonArray = new JSONArray(loadJSONFromAsset());
        Integer arrayPosition = Integer.parseInt(id);
        JSONObject jsonObject = jsonArray.getJSONObject(arrayPosition - 1);

        return jsonObject;
    }

}
