package com.patientandroid.chotahospital.doctorhelper;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.JsonWriter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Calendar;

public class addapointment extends AppCompatActivity {

    EditText contact_no,name , age;
    String gender,date1;
    RadioButton male , female , other;
    TextView textView;
    String[] parts;
    String year1 , month1 , day1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addapointment);

        contact_no = (EditText) findViewById(R.id.mobile);
        name = (EditText) findViewById(R.id.name);
        age = (EditText) findViewById(R.id.age);
        male = (RadioButton) findViewById(R.id.male);
        female = (RadioButton) findViewById(R.id.female);
        other = (RadioButton) findViewById(R.id.other);
        textView = (TextView) findViewById(R.id.appointmentTime);
        textView.setText(getIntent().getStringExtra("Time-Slot"));

        date1 = getIntent().getStringExtra("Date");
        parts = date1.split("-");
        year1 = parts[2];
        month1= parts[1];
        day1= parts[0];
    }

    /**
     * called when the add new patient button is presssed
     * @param view
     */

    public void add(View view){

        String contact = contact_no.getText().toString();
        String pat_name = name.getText().toString();
        String pat_age = age.getText().toString();


        gender = "";
        if(male.isChecked()){
            gender = "Male";
        }
        else if(female.isChecked()){
            gender = "Female";
        }
        else{
            gender = "Other";
        }

        String errormsg = "";
        if(contact.equals("")){
            errormsg = errormsg + "Mobile Number field is empty ";
            //Toast.makeText(this , "Mobile Number field is empty" , Toast.LENGTH_LONG).show();
        }
        if(contact.length() != 10 && !contact.equals("")){

            errormsg = errormsg+"\n" + "Mobile Number incorrect ";
            //Toast.makeText(this , "Mobile Number incorrect" , Toast.LENGTH_LONG).show();
        }
        if(pat_name.equals("")){
            errormsg = errormsg +"\n" + "Name field is empty ";
            //Toast.makeText(this , "Enter your name" , Toast.LENGTH_LONG).show();
        }
        if(pat_age.equals("")){
            errormsg = errormsg +"\n" + "Age field is empty ";
            //Toast.makeText(this , "Enter your age" , Toast.LENGTH_LONG).show();
        }
        if(pat_age.equals("0")){
            errormsg = errormsg +"\n" + "Invalid age ";
            //Toast.makeText(this , "Enter a valid age" , Toast.LENGTH_LONG).show();
        }
        if(gender.equals("")){
            errormsg = errormsg +"\n " + "Gender not selected";
            //Toast.makeText(this , "Fill the gender field" , Toast.LENGTH_LONG).show();
        }

        if(!errormsg.equals("")){
            Toast.makeText(this , errormsg , Toast.LENGTH_LONG).show();
        }
        else{
            try
            {
                //Read the data from json file.
                InputStream is=null;
                try {
                    is = new FileInputStream(Environment.getExternalStorageDirectory() + File.separator+
                            "Android"+File.separator +"data"+File.separator+"com.chotahospital.appointment"+File.separator+ "event.json");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                InputStream stream = is;
                BufferedReader reader=null ;
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                try {
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                JSONObject json =null;
                try {
                    json=new JSONObject(buffer.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(json==null){
                    json=new JSONObject();
                }
                

                JSONObject datejson = new JSONObject();

                datejson.put("Mobile" , contact);
                datejson.put("Name" , pat_name);
                datejson.put("Age",pat_age);
                datejson.put("Gender",gender);
                datejson.put("Time",textView.getText().toString());

                if(json.has(date1)){
                    //jsonObject.put(date1 , datejson);
                    Log.e("added to same date",date1);
                    json.getJSONArray(date1).put(datejson);
                    writeToFile(json);
                }
                else{
                    Log.e("created new row",date1);
                    JSONArray identity = new JSONArray();
                    identity.put(datejson);
                    json.put(date1 , identity);
                    writeToFile(json);
                }

                Toast.makeText(this , "New appointment added",Toast.LENGTH_LONG).show();

                Intent intent = new Intent(this,appointmentSchedule.class);
                intent.putExtra("Day",day1);
                intent.putExtra("Month",month1);
                intent.putExtra("Year",year1);

                startActivity(intent);
                this.finish();
            }
            catch (SQLException e)
            {
                Toast.makeText(this ,"Patient with this mobile number already exists , enter a different mobile number",Toast.LENGTH_LONG).show();
                Log.e("doc_helper", e.toString());
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Write data to json file
     * @param data
     */
    private void writeToFile(JSONObject data) {
        FileWriter fw = null;
        try {
            fw = new FileWriter(Environment.getExternalStorageDirectory() + File.separator+
                    "Android"+File.separator +"data"+File.separator+"com.chotahospital.appointment"+File.separator+ "event.json");
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedWriter bw = new BufferedWriter(fw);
        try {
            bw.write(data.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * read data from json file
     * @return String , entire information in string format , stored on json file
     */
    private String readFromFile() {

        InputStream is=null;
        try {
            is = new FileInputStream(Environment.getExternalStorageDirectory() + File.separator+
                    "Android"+File.separator +"data"+File.separator+"com.chotahospital.appointment"+File.separator+ "event.json");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        InputStream stream = is;
        BufferedReader reader=null ;
        reader = new BufferedReader(new InputStreamReader(stream));
        StringBuffer buffer = new StringBuffer();
        String line = "";
        try {
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return buffer.toString();
    }

    /**
     * returns to appointmentSchedule activity when back button is pressed.
     */

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this,appointmentSchedule.class);
        intent.putExtra("Day",day1);
        intent.putExtra("Month",month1);
        intent.putExtra("Year",year1);
        startActivity(intent);
        finish();

    }

}
