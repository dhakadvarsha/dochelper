package com.patientandroid.chotahospital.doctorhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class db_handler extends SQLiteOpenHelper {
    private static final int dbVer = 1;
    public db_handler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, doc_helper, factory, dbVer);
    }

    private static final String ID_NUM_BG = "ID_NUM_BG";
    private static final String ID_NUM_I = "ID_NUM_I";
    private static final String ID_NUM = "ID_NUM";
    private static final String typeOfRow = "typeOfRow";
    private static final String doc_helper = "doc_helper";
    private static final String doc_profile = "doc_profile";
    private static final String pat_info = "pat_info";
    private static final String blood_glucose = "blood_glucose";
    private static final String insulin = "insulin";
    private static final String medication = "medication";

    private static final String d_name = "d_name";
    private static final String d_sex = "d_sex";
    private static final String d_contact_no = "d_contact_no";
    private static final String d_speciality = "d_speciality";
    private static final String d_email_id = "d_email_id";
    private static final String d_age = "d_age";
    private static final String d_work_exp = "d_work_exp";
    // private static final Blob d_profile="profile_pic"

    private static final String p_contact_no = "p_contact_no";
    private static final String p_name = "p_name";
    private static final String p_age = "p_age";
    private static final String p_gender="p_gender";
    private static final String p_email_id = "p_email_id";

    private static final String bg_no = "g_no";
    private static final String bg_value = "bg_value";
    //private static final String bg_date = "bg_date";
    private static final String bg_type = "bg_type";
    private static final String bg_comments = "bg_comments";

    private static final String i_no = "i_no";
    private static final String i_intake_value = "i_intake_value";
    private static final String i_s_date = "i_s_date";
    private static final String i_e_date = "i_e_date";
    private static final String i_type = "i_type";
    private static final String i_breakfast = "i_breakfast";
    private static final String i_lunch = "i_lunch";
    private static final String i_snacks = "i_snacks";
    private static final String i_dinner = "i_dinner";
    private static final String i_night = "i_night";
    private static final String i_reminder = "i_reminder";

    private static final String m_no = "m_no";
    private static final String m_name = "m_name";
    private static final String m_quantity = "quantity";
    private static final String m_s_date = "m_s_date";
    private static final String m_e_date = "m_e_date";
    private static final String m_time = "m_time";

    private static final String pat_id = "pat_id";
    private static final String pat_appointment_hour = "pat_appointment_hour";
    private static final String pat_appointment_minute = "pat_appointment_minute";
    private static final String pat_name = "pat_name";
    private static final String pat_mob = "pat_mob";
    private static final String pat_age = "pat_age";
    private static final String pat_gender = "pat_gender";

    public db_handler(Context context) {
        super(context, doc_helper, null, dbVer);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String qry_doc = "CREATE TABLE " + doc_profile + " (" +
                d_contact_no + " TEXT PRIMARY KEY, " +
                d_name + " TEXT, " +
                d_sex + " TEXT, " +
                d_speciality + " TEXT, " +
                d_email_id + " TEXT, " +
                d_work_exp + " TEXT, " +
                d_age + " TEXT " +
                ");";
        String qry_pat = "CREATE TABLE " + pat_info + " (" +
                p_contact_no + " TEXT PRIMARY KEY, " +
                p_name + " TEXT, " +
                p_gender + " TEXT, " +
                p_age + " TEXT, " +
                p_email_id + " TEXT " +
                ");";

        db.execSQL(qry_doc);
        db.execSQL(qry_pat);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + doc_profile);
        db.execSQL("DROP TABLE IF EXISTS " + pat_info);
        onCreate(db);
    }


    public void createDynamicPatientInfoBloodGlucose(String contact){
        SQLiteDatabase db = getWritableDatabase();

        String patTable = "CREATE TABLE bg_" + contact + "(" +
                ID_NUM_BG + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                bg_value + " TEXT, " +
                bg_type + " TEXT, " +
                bg_comments + " TEXT " +
                ");";

        db.execSQL(patTable);
        db.close();
    }


    public void createDynamicPatientInfoInsulin(String contact){
        SQLiteDatabase db = getWritableDatabase();

        String patTable = "CREATE TABLE i_" + contact + "(" +
                ID_NUM_I + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                i_intake_value + " TEXT, " +
                i_s_date + " DATE, " +
                i_e_date + " DATE, " +
                i_type + " TEXT, " +
                i_breakfast + " BOOLEAN, " +
                i_lunch + " BOOLEAN, " +
                i_snacks + " BOOLEAN, " +
                i_dinner + " BOOLEAN, " +
                i_night + " BOOLEAN, " +
                i_reminder + " BOOLEAN " +
                ");";

        db.execSQL(patTable);
        db.close();
    }

    public void createDynamicPatientInfoAppointment(String date){
        SQLiteDatabase db = getWritableDatabase();

        String qry_appointment = "CREATE TABLE appointment_" + date + " (" +
                pat_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                pat_name + " TEXT, " +
                pat_appointment_hour + " TEXT, " +
                pat_appointment_minute + " TEXT, " +
                pat_gender + " TEXT, " +
                pat_age + " TEXT, " +
                pat_mob + " TEXT " +
                ");";

        db.execSQL(qry_appointment);
        db.close();
    }

    public void setBlood_glucose(Glucose G_data,String table_name) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(bg_value, G_data.bg_value);
        //values.put(bg_date, G_data.bg_date);
        values.put(bg_type, G_data.bg_type);
        values.put(bg_comments, G_data.bg_comments);
        db.insert("bg_"+table_name, null, values);
        db.close();
    }

    public void setPat_appointment(pat_appointment appointment,String date) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(pat_name, appointment.pat_name);
        values.put(pat_mob, appointment.pat_mob);
        values.put(pat_age, appointment.pat_age);
        values.put(pat_appointment_hour, appointment.pat_appointment_hour);
        values.put(pat_appointment_minute, appointment.pat_appointment_minute);
        values.put(pat_gender, appointment.pat_gender);
        db.insert("appointment_"+date, null, values);
        db.close();
    }

    public void UpdatePat_appointment(pat_appointment appointment,String date,Integer id) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(pat_name, appointment.pat_name);
        values.put(pat_mob, appointment.pat_mob);
        values.put(pat_age, appointment.pat_age);
        values.put(pat_appointment_hour, appointment.pat_appointment_hour);
        values.put(pat_appointment_minute, appointment.pat_appointment_minute);
        values.put(pat_gender, appointment.pat_gender);
        db.update("appointment_"+date, values, pat_id + " = ?", new String[] { String.valueOf(id) });
        db.close();
    }

    public void updateBlood_glucose(Glucose G_data,String contact,Integer id){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(bg_value, G_data.bg_value);
        //values.put(bg_date, G_data.bg_date);
        values.put(bg_type, G_data.bg_type);
        values.put(bg_comments, G_data.bg_comments);
        db.update("bg_"+contact, values, ID_NUM_BG + " = ?", new String[] { String.valueOf(id) });
        db.close();
    }

    public void  deleteBlood_glucose(Integer id,String table_name){
        SQLiteDatabase db = getWritableDatabase();
        db.delete("bg_"+table_name , ID_NUM_BG + " = ?",new String[] { String.valueOf(id) });
        db.close();
    }

    public void setInsulin(Insulin I_data,String table_name) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(i_intake_value, I_data.i_intake_value);
        values.put(i_s_date, I_data.i_s_date);
        values.put(i_e_date, I_data.i_e_date);
        values.put(i_type, I_data.i_type);
        values.put(i_breakfast, I_data.i_breakfast);
        values.put(i_lunch, I_data.i_lunch);
        values.put(i_snacks, I_data.i_snacks);
        values.put(i_dinner, I_data.i_dinner);
        values.put(i_night, I_data.i_night);
        values.put(i_reminder, I_data.i_reminder);
        db.insert("i_"+table_name, null, values);
        db.close();
    }

    public void updateInsulin(Insulin I_data,String table_name,Integer id) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(i_intake_value, I_data.i_intake_value);
        values.put(i_s_date, I_data.i_s_date);
        values.put(i_e_date, I_data.i_e_date);
        values.put(i_type, I_data.i_type);
        values.put(i_breakfast, I_data.i_breakfast);
        values.put(i_lunch, I_data.i_lunch);
        values.put(i_snacks, I_data.i_snacks);
        values.put(i_dinner, I_data.i_dinner);
        values.put(i_night, I_data.i_night);
        values.put(i_reminder, I_data.i_reminder);
        db.update("i_"+table_name, values , ID_NUM_I + " = ?", new String[] { String.valueOf(id) });
        db.close();
    }

    public void deleteInsulin(Integer id,String table_name){
        SQLiteDatabase db = getWritableDatabase();
        db.delete("i_"+table_name , ID_NUM_I + " = ?",new String[] { String.valueOf(id) });
        db.close();
    }
    public void createDynamicPatientInfo(String contact){
        String patTable = "CREATE TABLE pat_" + contact + " (" +
                ID_NUM + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                bg_value + " TEXT, " +
                //bg_date + " DATE, " +
                bg_type + " TEXT, " +
                bg_comments + " TEXT, " +
                i_intake_value + " TEXT, " +
                i_s_date + " DATE, " +
                i_e_date + " DATE, " +
                i_breakfast + " BOOLEAN, " +
                i_lunch + " BOOLEAN, " +
                i_snacks + " BOOLEAN, " +
                i_dinner + " BOOLEAN, " +
                i_night + " BOOLEAN, " +
                m_name + " TEXT, " +
                m_quantity + " TEXT, " +
                m_s_date + " TEXT, " +
                m_e_date + " TEXT, " +
                m_time + " BOOLEAN, " +
                typeOfRow + " TEXT " +
                ");";
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(patTable);

    }
//
    public void addDocInfo(DocInfo D_data,String g) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(d_name, D_data.d_name);
        values.put(d_sex, D_data.d_sex);
        values.put(d_contact_no, D_data.d_contact_no);
        values.put(d_speciality, D_data.d_speciality);
        values.put(d_email_id, D_data.d_email_id);
        values.put(d_age, D_data.d_age);
        values.put(d_work_exp, D_data.d_work_exp);
        String where ="d_contact_no=?";
        String[] whereArgs = new String[] {String.valueOf(g)};
        db.update(doc_profile, values ,where,whereArgs);
        db.close();
    }

    public void addPatInfo(PatInfo P_data) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(p_name, P_data.p_name);
        values.put(p_contact_no, P_data.p_contact_no);
        values.put(p_email_id, P_data.p_email_id);
        values.put(p_age,P_data.p_age);
        values.put(p_gender,P_data.p_gender);
        db.insert(pat_info, null, values);
        db.close();
    }


    public void updatePatInfo(PatInfo P_data,String id) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(p_name, P_data.p_name);
        values.put(p_contact_no, P_data.p_contact_no);
        values.put(p_email_id, P_data.p_email_id);
        values.put(p_age,P_data.p_age);
        values.put(p_gender,P_data.p_gender);
    }
    public Cursor getinfo_patient(SQLiteDatabase db) {
        Cursor cursor;
        String[] projections = {p_contact_no , p_name , p_email_id , p_age , p_gender};//table's attribute
        cursor = db.query(pat_info, projections, null, null, null, null, null);
        return cursor;
    }

    public static Cursor get_cursor(SQLiteDatabase db) {
        //    Cursor cursor = sqLiteDatabase.query(
        //          tableName, tableColumns, whereClause, whereArgs, groupBy, having, orderBy);
        Cursor cursor;
        String[] projections = {p_name, p_age ,p_gender,p_contact_no,p_email_id};
        cursor = db.query(pat_info, projections, null, null, null, null, null);
        return cursor;
    }

    public static Cursor get_doc_profile(SQLiteDatabase db) {
        Cursor cursor;
        String[] projections = {d_contact_no ,d_name,d_sex,d_speciality,d_email_id,d_age,d_work_exp};
        cursor = db.query(doc_profile, projections, null, null, null, null, null);
        return cursor;
    }
    //med mod
//    public void setMedication(Medication M_data){
//        SQLiteDatabase db = getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(m_name, M_data.m_name);
//        values.put(m_quantity, M_data.m_quantity);
//        values.put(m_s_date, M_data.m_s_date);
//        values.put(m_e_date, M_data.m_e_date);
//        values.put(m_time, M_data.m_time);
//        db.insert("pat_"+M_data.ID_NUM, null, values);
//        db.close();
//    }
//    public void delMed(Medication M_data, String ss){
//        SQLiteDatabase db = getWritableDatabase();
//        String where ="m_name="+"\""+ss+"\"";
//        db.delete("pat_"+M_data.ID_NUM, where, null);
//        db.close();
//    }
    //med mod ends
//    public void updateMed(Medication M_data,String ss){
//        SQLiteDatabase db = getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(m_name, M_data.m_name);
//        values.put(m_quantity, M_data.m_quantity);
//        values.put(m_s_date, M_data.m_s_date);
//        values.put(m_e_date, M_data.m_e_date);
//        values.put(m_time, M_data.m_time);
//        String where ="m_name=?";
//        String[] whereArgs = new String[] {String.valueOf(ss)};
//        db.update("pat_"+M_data.ID_NUM, values ,where,whereArgs);
//        db.close();
//    }
    public void deletePatInfo(String id){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(pat_info , p_contact_no + " = ?",new String[] { String.valueOf(id) });
        String droptable1 = "DROP TABLE bg_"+id;
        String droptable2 = "DROP TABLE i_"+id;
        String droptable3 = "DROP TABLE pat_"+id;
        db.execSQL(droptable1);
        db.execSQL(droptable2);
        db.execSQL(droptable3);
        db.close();
    }

    public void UpdateTableName(String newContact , String oldContact){
        SQLiteDatabase db = getWritableDatabase();
        String update_table_name_bg = "ALTER TABLE bg_"+oldContact+" RENAME TO bg_"+newContact;
        String update_table_name_i = "ALTER TABLE i_"+oldContact+" RENAME TO i_"+newContact;
        String update_table_name_med = "ALTER TABLE pat_"+oldContact+" RENAME TO pat_"+newContact;
        db.execSQL(update_table_name_bg);
        db.execSQL(update_table_name_i);
        db.execSQL(update_table_name_med);
        db.close();
    }
    public static Cursor get_medication(SQLiteDatabase db,String contact) {
        Cursor cursor;
        String[] projections = {ID_NUM,m_name ,m_quantity,m_s_date,m_e_date,m_time,};
        cursor = db.query("pat_"+contact, projections,null, null, null, null, null);
        return cursor;
    }

    public Cursor getinfo_glucose(SQLiteDatabase db,String table_name){
        Cursor cursor;
        String[] projections = {ID_NUM_BG , bg_value , bg_type , bg_comments};
        cursor = db.query("bg_"+table_name , projections , null, null, null, null,null);
        return  cursor;
    }

    public Cursor getinfo_insulin(SQLiteDatabase db,String table_name){
        Cursor cursor;
        String[] projections = {ID_NUM_I,i_intake_value,i_s_date,i_e_date,i_type,i_breakfast,i_lunch,i_snacks,i_dinner,i_night,i_reminder};
        cursor = db.query("i_"+table_name, projections, null, null, null, null,null);
        return  cursor;
    }

    public Cursor getinfo_appointment(SQLiteDatabase db,String date,int starttime , int endtime){
        Cursor cursor;
        String[] projections = {pat_id,pat_mob,pat_name,pat_age,pat_gender,pat_appointment_hour,pat_appointment_minute};
        cursor = db.query("appointment_"+date, projections, pat_appointment_hour + ">=" + starttime +" AND "+pat_appointment_hour + "<" + endtime, null, null, null,null);
        return  cursor;
    }
}
