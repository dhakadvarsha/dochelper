package com.patientandroid.chotahospital.doctorhelper;

/**
 * Created by varsh on 28-06-2016.
 */
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseDatabaseUtil {

    private static FirebaseDatabase database;

    public static FirebaseDatabase getDatabase() {
        if (database == null) {
            database = FirebaseDatabase.getInstance();
            database.setPersistenceEnabled(true);
        }

        return database;

    }

}
