package com.patientandroid.chotahospital.doctorhelper;

/**
 * Created by Lalit Gautam on 6/1/2016.
 */
public class Glucose {

    public String bg_value;
    public String bg_date;
    public String bg_type;
    public String bg_comments;

    public Glucose() {
    }

    public String getBg_value() {
        return bg_value;
    }

    public String getBg_date() {
        return bg_date;
    }

    public String getBg_type() {
        return bg_type;
    }

    public String getBg_comments() {
        return bg_comments;
    }

    public Glucose(String bg_no, String bg_value, String bg_date, String bg_type, String bg_comments) {
        this.bg_value = bg_value;
        this.bg_date = bg_date;
        this.bg_type = bg_type;
        this.bg_comments = bg_comments;
    }
}
