package com.patientandroid.chotahospital.doctorhelper;

/**
 * Created by Lalit Gautam on 6/1/2016.
 */
public class Insulin {

    public String i_intake_value;
    public String i_s_date;
    public String i_e_date;
    public String i_type;
    public Integer i_breakfast;
    public Integer i_lunch;
    public Integer i_snacks;
    public Integer i_dinner;
    public Integer i_night;
    public Integer i_reminder;

    public Insulin() {
    }

    public String getI_intake_value() {
        return i_intake_value;
    }

    public String getI_s_date() {
        return i_s_date;
    }

    public String getI_e_date() {
        return i_e_date;
    }

    public String getI_type() {
        return i_type;
    }

    public Integer getI_breakfast() {
        return i_breakfast;
    }

    public Integer getI_lunch() {
        return i_lunch;
    }

    public Integer getI_dinner() {
        return i_dinner;
    }

    public Integer getI_snacks() {
        return i_snacks;
    }

    public Integer getI_night() {
        return i_night;
    }

    public Integer getI_reminder() {
        return i_reminder;
    }

    public Insulin(String i_intake_value, String i_s_date, String i_e_date, String i_type, Integer i_breakfast, Integer i_lunch,
                   Integer i_snacks, Integer i_dinner, Integer i_night, Integer i_reminder) {
        this.i_intake_value = i_intake_value;
        this.i_s_date = i_s_date;
        this.i_e_date = i_e_date;
        this.i_type = i_type;
        this.i_breakfast = i_breakfast;
        this.i_lunch = i_lunch;
        this.i_snacks = i_snacks;
        this.i_dinner = i_dinner;
        this.i_night = i_night;
        this.i_reminder = i_reminder;
    }
}
