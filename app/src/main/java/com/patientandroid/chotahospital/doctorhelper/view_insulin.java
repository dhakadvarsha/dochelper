package com.patientandroid.chotahospital.doctorhelper;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class view_insulin extends AppCompatActivity {

    String contact_no;
    ListView listView;
    Adapter_insulin adapter_insulin;
    db_handler database;
    Cursor cursor;
    SQLiteDatabase sqLiteDatabase;
    Context context= this;
    Map<String,String> map = new HashMap<String, String>();
    List<Map<String,String>> idList = new ArrayList<Map<String, String>>();
    List<String> idDate = new ArrayList<>();

    Firebase root_ref;
    private static final String REFERENCE_URL = "https://chotahospital-ffc48.firebaseio.com/Patients";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_insulin);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("History");

        contact_no = getIntent().getStringExtra("Contact_No");

        Firebase.setAndroidContext(this);//Initialises firebase for using this android app
        root_ref = new Firebase(REFERENCE_URL);//Reference to data into firebase cloud.

        listView = (ListView) findViewById(R.id.list_view_insulin);
        adapter_insulin = new Adapter_insulin(getApplicationContext(), R.layout.insulin_row);
        listView.setAdapter(adapter_insulin);
        Firebase contact = root_ref.child(contact_no);
        Firebase patient = contact.child("Insulin");
        patient.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot DateData : dataSnapshot.getChildren() ) {

                    Log.e("Date is",DateData.getKey());
                    for (DataSnapshot Reading : DateData.getChildren()) {
                        Log.e("Reading is",Reading.getKey());
                        map.put(Reading.getKey(),DateData.getKey());
                        idList.add(map);
                        idDate.add(Reading.getKey());
                        Insulin InsulinReading = Reading.getValue(Insulin.class);
                        Insulin insulin = new Insulin();

                        insulin.i_intake_value = InsulinReading.getI_intake_value();
                        insulin.i_s_date = InsulinReading.getI_s_date();
                        insulin.i_e_date = InsulinReading.getI_e_date();
                        insulin.i_type = InsulinReading.getI_type();
                        insulin.i_breakfast = InsulinReading.getI_breakfast();
                        insulin.i_lunch = InsulinReading.getI_lunch();
                        insulin.i_snacks = InsulinReading.getI_snacks();
                        insulin.i_dinner = InsulinReading.getI_dinner();
                        insulin.i_night = InsulinReading.getI_night();
                        insulin.i_reminder = InsulinReading.getI_reminder();

                        adapter_insulin.add(InsulinReading);
                    }

                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                int i = 1;
                Map<String,String> removeFrom = idList.get(position);
                String OnDate = removeFrom.get(idDate.get(position));

                Intent i3 = new Intent(view_insulin.this , add_insulin.class);

                i3.putExtra("Value_id", i);
                i3.putExtra("Positionbg", idDate.get(position));
                i3.putExtra("DateUpdate",OnDate);
                i3.putExtra("Contact_No", contact_no);
                i3.putExtra("del",1);
                i3.putExtra("Position", getIntent().getIntExtra("Position", 0));

                startActivity(i3);
                view_insulin.this.finish();


            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final int pos = position;
                AlertDialog.Builder builder = new AlertDialog.Builder(view_insulin.this);
                builder.setCancelable(true);
                builder.setTitle("Do you want to delete?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        Map<String,String> removeFrom = idList.get(pos);
                        String OnDate = removeFrom.get(idDate.get(pos));

                        Firebase contact = root_ref.child(contact_no);
                        Firebase patient = contact.child("Insulin");
                        Firebase onDate = patient.child(OnDate);
                        onDate.child(idDate.get(pos)).removeValue();
                        ;

                        Toast.makeText(view_insulin.this,"Data Deleted", Toast.LENGTH_LONG).show();

                        Intent i2 = new Intent(view_insulin.this , view_insulin.class);
                        i2.putExtra("Contact_No",contact_no);
                        i2.putExtra("Position",getIntent().getIntExtra("Position",0));
                        startActivity(i2);
                        view_insulin.this.finish();
                    }

                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                    }

                });
                AlertDialog alert = builder.create();
                alert.show();

                return true;
            }
        });


    }


    public void fill_data(View view){
        Intent i = new Intent(this , add_insulin.class);
        i.putExtra("Contact_No",contact_no);
        i.putExtra("Position",getIntent().getIntExtra("Position",0));
        startActivity(i);
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_view_insulin, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent(this, view_patient.class);
            intent.putExtra("Contact_No",getIntent().getStringExtra("Contact_No"));
            intent.putExtra("Patient Name",getIntent().getStringExtra("Patient Name"));
            intent.putExtra("Position",getIntent().getIntExtra("Position",0));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this, view_patient.class);
        intent.putExtra("Contact_No",getIntent().getStringExtra("Contact_No"));
        startActivity(intent);
        finish();

    }
}