package com.patientandroid.chotahospital.doctorhelper;

public class Medication {

    public String m_name;
    public String m_quantity;
    public String m_s_date;
    public String m_e_date;
    public String m_time;

    public Medication() {
    }

    public Medication(String m_name, String m_quantity, String m_s_date, String m_e_date,
                      String m_time) {

        this.m_name = m_name;
        this.m_quantity = m_quantity;
        this.m_s_date = m_s_date;
        this.m_e_date = m_e_date;
        this.m_time = m_time;
    }

    public String getM_name() {
        return m_name;
    }

    public String getM_s_date() {
        return m_s_date;
    }

    public String getM_quantity() {
        return m_quantity;
    }

    public String getM_e_date() {
        return m_e_date;
    }

    public String getM_time() {
        return m_time;
    }

}