package com.patientandroid.chotahospital.doctorhelper;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

public class availablity_check extends AppCompatActivity {

    EditText date2;
    String item2;
    Spinner spinner2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_availablity_check);

        date2 = (EditText)findViewById(R.id.date);

        spinner2 = (Spinner) findViewById(R.id.appointmentTime);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.time_slot, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item2 = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    Calendar myCalendar1 = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar1.set(Calendar.YEAR, year);
            myCalendar1.set(Calendar.MONTH, monthOfYear);
            myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel1();
        }

    };

    /**
     * Date picker
     * @param view
     */
    public void Datepicker1(View view){
        new DatePickerDialog(this, date1, myCalendar1.get(Calendar.YEAR), myCalendar1.get(Calendar.MONTH), myCalendar1.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLabel1() {

        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        date2.setText(sdf.format(myCalendar1.getTime()));
    }

    /**
     * Cal;ed when check and Book button pressed.
     * @param view
     */

    public void checkandBook(View view){
        Log.e("Entered checkandBook","");

        //Read data from json file , stored at specified position
        Integer appointmentCounter = 0;
        InputStream is=null;
        try {
            is = new FileInputStream(Environment.getExternalStorageDirectory() + File.separator+
                    "Android"+File.separator +"data"+File.separator+"com.chotahospital.appointment"+File.separator+ "event.json");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        InputStream stream = is;
        BufferedReader reader=null ;
        reader = new BufferedReader(new InputStreamReader(stream));
        StringBuffer buffer = new StringBuffer();
        String line = "";
        try {
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(buffer.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("date is",date2.getText().toString());

        Iterator<String> keys = jsonObject != null ? jsonObject.keys() : null;
        ArrayList<String> allDates = new ArrayList<>();

        if (keys != null) {
            while (keys.hasNext()){
                String key = keys.next();
                allDates.add(key);//all the dates from database is extracted.
            }
        }

        for(int i = 0 ; i<allDates.size();++i){
            Log.e("Date..... is...",allDates.get(i));
        }

        if (jsonObject.has(date2.getText().toString())) {
            Log.e("Entered the loop","");


            try {
                JSONArray array = jsonObject.getJSONArray(date2.getText().toString());
                for (int i = 0; i < array.length(); ++i) {
                    if ((array.getJSONObject(i).get("Time")).toString().equals(item2)) {
                        appointmentCounter++;
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        Log.e("appointmentCounter",appointmentCounter.toString());
        if(appointmentCounter < 3){
            Toast.makeText(this,"Available",Toast.LENGTH_LONG).show();

        }
        else{
            Toast.makeText(this,"Not Available",Toast.LENGTH_LONG).show();
        }

     }

    /**
     * called when the next button is pressed.
     * @param view
     */

    public void next(View view){
        String[] parts = (date2.getText().toString()).split("-");
        String year1 = parts[2] , month1= parts[1] , day1= parts[0];

        Intent intent = new Intent(this , appointmentSchedule.class);
        intent.putExtra("Go",1);
        intent.putExtra("Day",day1);
        intent.putExtra("Month",month1);
        intent.putExtra("Year",year1);
        intent.putExtra("Time-Slot",item2);

        startActivity(intent);
        this.finish();
    }

    //returns back to appointmnentSchedule activty when back button is pressed.
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this, appointmentSchedule.class);
        startActivity(intent);
        finish();

    }
}
