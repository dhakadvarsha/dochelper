package com.patientandroid.chotahospital.doctorhelper;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class add_schedule extends AppCompatActivity {

    EditText contact_no,name , age;
    String gender,old_id;
    db_handler database;
    SQLiteDatabase sqLiteDatabase;
    Context context = this;
    Cursor cursor;
    Integer fragment_type = 0 , starttime , endtime , pos;
    RadioButton male , female , other;
    db_handler db;
    Integer i=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_schedule);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("New Record");

        contact_no = (EditText) findViewById(R.id.contact);
        name = (EditText) findViewById(R.id.name);
        age = (EditText) findViewById(R.id.age);
        male = (RadioButton) findViewById(R.id.male);
        female = (RadioButton) findViewById(R.id.female);
        other = (RadioButton) findViewById(R.id.other);

        fragment_type = getIntent().getIntExtra("Fragment_type",0);
        Log.e("fragment type is",fragment_type.toString());
        switch (fragment_type){
            case 0:
                starttime = 8;
                endtime = 12;
                break;
            case 1:
                starttime = 12;
                endtime = 17;
                break;
            case 2:
                starttime = 17;
                endtime = 20;
                break;
        }


        i = getIntent().getIntExtra("Update",0);
        if(i == 1){
            setTitle("Update Record");
            // Toast.makeText(add_patient.this , "Entered update function ",Toast.LENGTH_SHORT).show();
            pos = getIntent().getIntExtra("Position",0);
            database = new db_handler(context);
            sqLiteDatabase = database.getReadableDatabase();
            cursor = database.getinfo_appointment(sqLiteDatabase,getIntent().getStringExtra("Date"),starttime,endtime);
            cursor.moveToPosition(pos);

            contact_no.setText(cursor.getString(1));
            name.setText(cursor.getString(2));
            age.setText(cursor.getString(3));

            String gender = cursor.getString(4);
            if(gender.equals("Male")){
                male.setChecked(true);
            }
            else if(gender.equals("Female")){
                female.setChecked(true);
            }
            else if(gender.equals("Other")){
                other.setChecked(true);
            }
            database.close();
        }


    }

    public void add_p(View view){

        String contact = contact_no.getText().toString();
        String pat_name = name.getText().toString();
        String pat_age = age.getText().toString();


        if(male.isChecked()){
            gender = "Male";
        }
        else if(female.isChecked()){
            gender = "Female";
        }
        else{
            gender = "Other";
        }

        pat_appointment appointment = new pat_appointment();
        appointment.pat_mob = contact;
        appointment.pat_name = pat_name;
        appointment.pat_age = pat_age;
        appointment.pat_gender = gender;

        db = new db_handler(this);

        String errormsg = "";
        if(contact.equals("")){
            errormsg = errormsg + "Mobile Number field is empty ";
            //Toast.makeText(this , "Mobile Number field is empty" , Toast.LENGTH_LONG).show();
        }
        if(contact.length() != 10){

            errormsg = errormsg+"\n" + "Mobile Number incorrect ";
            //Toast.makeText(this , "Mobile Number incorrect" , Toast.LENGTH_LONG).show();
        }
        if(pat_name.equals("")){
            errormsg = errormsg +"\n" + "Name field is empty ";
            //Toast.makeText(this , "Enter your name" , Toast.LENGTH_LONG).show();
        }
        if(pat_age.equals("")){
            errormsg = errormsg +"\n" + "Age field is empty ";
            //Toast.makeText(this , "Enter your age" , Toast.LENGTH_LONG).show();
        }
        if(pat_age.equals("0")){
            errormsg = errormsg +"\n" + "Invalid age ";
            //Toast.makeText(this , "Enter a valid age" , Toast.LENGTH_LONG).show();
        }
        if(gender.equals("")){
            errormsg = errormsg +"\n " + "Gender not selected";
            //Toast.makeText(this , "Fill the gender field" , Toast.LENGTH_LONG).show();
        }

        if(!errormsg.equals("")){
            Toast.makeText(this , errormsg , Toast.LENGTH_LONG).show();
        }
        else if(i==1){

                db.UpdatePat_appointment(appointment ,getIntent().getStringExtra("Date") , cursor.getInt(0) );
                Toast.makeText(this , "Patient updated info" , Toast.LENGTH_LONG).show();
                Intent i = new Intent(this , daySchedule.class);
                startActivity(i);
                this.finish();

        }
        else{
                database = new db_handler(context);
                sqLiteDatabase = database.getReadableDatabase();
                cursor = database.getinfo_appointment(sqLiteDatabase,getIntent().getStringExtra("Date"),starttime,endtime);

                Log.e("StartTime" , starttime.toString());
                appointment.pat_appointment_hour = starttime;
                appointment.pat_appointment_minute = starttime + 15;
                Log.e("Starttime",starttime.toString());
                /*if(cursor.getCount() > 0){
                    cursor.moveToPrevious();
                    appointment.pat_appointment_hour = cursor.getInt(5);
                    Log.e("Starttime prevoius",String.valueOf(cursor.getInt(5)));
                    appointment.pat_appointment_minute = cursor.getInt(6);
                    Log.e("minute prevoius ",String.valueOf(cursor.getInt(6)));
                    if(cursor.getInt(6) < (cursor.getInt(6) + 15)){
                        appointment.pat_appointment_hour++;
                        appointment.pat_appointment_minute = 60-cursor.getInt(6);
                    }
                }
                */
                db.setPat_appointment(appointment,getIntent().getStringExtra("Date"));
                Toast.makeText(this , "New Patient added" , Toast.LENGTH_LONG).show();
                Intent i = new Intent(this , daySchedule.class);
                i.putExtra("Date" , getIntent().getStringExtra("Date"));
                startActivity(i);
                this.finish();
                database.close();

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_insulin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        /** if (id == R.id.editprofile) {
         Toast.makeText(this , "About to call Profile.class" , Toast.LENGTH_LONG).show();
         Intent intent = new Intent(add_insulin.this, Profile.class);
         intent.putExtra("Profile Update", 1);
         startActivity(intent);
         this.finish();
         }
         */

        if(id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            this.finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this, daySchedule.class);
        startActivity(intent);
        finish();

    }
}
