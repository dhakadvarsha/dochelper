package com.patientandroid.chotahospital.doctorhelper;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuWrapperFactory;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;


public class add_med extends AppCompatActivity {

    String s,flag;
    String ss;
    int f=1;
    int pob=1;
    int pol=1;
    int prel=1;
    int pred=1;
    int pod=1;
    int n=1;

    Firebase root_ref;
    private static final String REFERENCE_URL = "https://chotahospital-ffc48.firebaseio.com/Medication";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_med);
        s=getIntent().getStringExtra("Contact_No");

        final EditText tname=(EditText) (findViewById(R.id.med_name));
        final EditText tsd=(EditText) findViewById(R.id.med_start);
        final EditText tend=(EditText) findViewById(R.id.med_end);
        final Spinner meddose_=(Spinner)findViewById(R.id.med_quantity);

        Firebase.setAndroidContext(this);//Initialises firebase for using this android app
        root_ref = new Firebase(REFERENCE_URL);//Reference to data into firebase cloud.

        {
        CheckedTextView chkBoxFast = (CheckedTextView) findViewById(R.id.X_fasting);
        chkBoxFast.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                ((CheckedTextView) v).toggle();
                f*=-1;
            }
        });
        CheckedTextView chkBoxPB = (CheckedTextView) findViewById(R.id.X_post_b);
        chkBoxPB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                pob*=-1;
                ((CheckedTextView) v).toggle();
            }
        });
        CheckedTextView chkBoxPrl = (CheckedTextView) findViewById(R.id.X_pre_l);
        chkBoxPrl.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                prel*=-1;
                ((CheckedTextView) v).toggle();
            }
        });
        CheckedTextView chkBoxPol = (CheckedTextView) findViewById(R.id.X_post_l);
        chkBoxPol.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                pol*=-1;
                ((CheckedTextView) v).toggle();
            }
        });
        CheckedTextView chkBoxPrd = (CheckedTextView) findViewById(R.id.X_pre_d);
        chkBoxPrd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                pred*=-1;
                ((CheckedTextView) v).toggle();
            }
        });
        CheckedTextView chkBoxPod = (CheckedTextView) findViewById(R.id.X_post_d);
        chkBoxPod.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                pod*=-1;
                ((CheckedTextView) v).toggle();
            }
        });
        CheckedTextView chkBoxN = (CheckedTextView) findViewById(R.id.X_night);
        chkBoxN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                n*=-1;
                ((CheckedTextView) v).toggle();
            }
        });}

        if(getIntent().getIntExtra("DateUpdate",0) == 1){
            Firebase contact = root_ref.child(getIntent().getStringExtra("patid"));
            Firebase onUpdateid = contact.child(getIntent().getStringExtra("UpdateAtid"));

            onUpdateid.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                                Log.e("Read is",dataSnapshot.getKey());
                                Medication ReadData = dataSnapshot.getValue(Medication.class);

                                tname.setText(ReadData.getM_name());
                                tsd.setText(ReadData.getM_s_date());
                                tend.setText(ReadData.getM_e_date());

                                String[] schedule2 = {"1/2 Tablet", "1 Tablet" , "1 Spoon" , "2 Spoon"};
                                int o = 0;
                                for (int j = 0; j < schedule2.length; j++) {
                                    if (ReadData.getM_quantity().equals(schedule2[j])) {
                                        o = j;
                                    }
                                }

                                meddose_.setSelection(o);

                                String dummy = ReadData.getM_time();
                                if(dummy==null)
                                {
                                    dummy="";
                                }
                                if(dummy.length()>0)
                                {
                                    if (dummy.charAt(0) == '-') {
                                        CheckedTextView chkBoxPod = (CheckedTextView) findViewById(R.id.X_fasting);
                                        chkBoxPod.toggle();
                                        f *= -1;
                                    }
                                    if (dummy.charAt(1) == '-') {
                                        CheckedTextView chkBoxPod = (CheckedTextView) findViewById(R.id.X_post_b);
                                        chkBoxPod.toggle();
                                        pob *= -1;
                                    }
                                    if (dummy.charAt(2) == '-') {
                                        CheckedTextView chkBoxPod = (CheckedTextView) findViewById(R.id.X_pre_l);
                                        chkBoxPod.toggle();
                                        prel *= -1;
                                    }
                                    if (dummy.charAt(3) == '-') {
                                        CheckedTextView chkBoxPod = (CheckedTextView) findViewById(R.id.X_post_l);
                                        chkBoxPod.toggle();
                                        pol *= -1;
                                    }
                                    if (dummy.charAt(4) == '-') {
                                        CheckedTextView chkBoxPod = (CheckedTextView) findViewById(R.id.X_pre_d);
                                        chkBoxPod.toggle();
                                        pred *= -1;
                                    }
                                    if (dummy.charAt(5) == '-') {
                                        CheckedTextView chkBoxPod = (CheckedTextView) findViewById(R.id.X_post_d);
                                        chkBoxPod.toggle();
                                        pod *= -1;
                                    }
                                    if (dummy.charAt(6) == '-') {
                                        CheckedTextView chkBoxPod = (CheckedTextView) findViewById(R.id.X_night);
                                        chkBoxPod.toggle();
                                        n *= -1;
                                    }
                                }

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }

        ArrayAdapter bb =ArrayAdapter.createFromResource(this,R.array.dose_val,android.R.layout.simple_spinner_dropdown_item);
        meddose_.setAdapter(bb);

        meddose_.setSelection(bb.getPosition(getIntent().getStringExtra("qt")));

        ss = tname.getText().toString();
        if(getIntent().getStringExtra("flag").equals("1")){
            flag="1";
        }
        else if(getIntent().getStringExtra("flag").equals("0")){
            flag="0";
        }

        meddose_.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setGravity(Gravity.CENTER);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    public void onClick(View v){
        Medication sample=new Medication();

        boolean t=true;

        TextView tname=(TextView)(findViewById(R.id.med_name));
        TextView tsd=(TextView)findViewById(R.id.med_start);
        TextView tend=(TextView)findViewById(R.id.med_end);

        if(tname.getText().toString().length()<=0)
        {
            t=false;
            new AlertDialog.Builder(this)
                    .setTitle("Invalid Entry")
                    .setMessage("Medicine name cannot be left blank")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        String sd=tsd.getText().toString();
        String ed=tend.getText().toString();
        int sl=sd.length();
        int el=ed.length();
        int i=0,j=0;
        int num1=0;
        int num2=0;

        while (i<el){
            if(ed.charAt(el-1-i)=='-'){
                i++;
                break;
            }
            else{
                num1= (int) (num1+Integer.parseInt(String.valueOf(ed.charAt(el-1-i)))*Math.pow(10,i));
            }
            i++;
        }
        while (j<sl){
            if(sd.charAt(sl-1-j)=='-'){
                j++;
                break;
            }
            else {
                num2= (int) (num2+Integer.parseInt(String.valueOf(sd.charAt(sl-1-j)))*Math.pow(10,j));
            }
            j++;
        }
        if(num2>num1){
            t=false;
            new AlertDialog.Builder(this)
                    .setTitle("Invalid Entry")
                    .setMessage("Ending date has to be greater than start date")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        int num11=0;
        int num22=0;
        int i1=0,j1=0;

        while (i<el){
            if(ed.charAt(el-1-i)=='-'){
                i++;
                break;
            }
            else{
                num11= (int) (num11+Integer.parseInt(String.valueOf(ed.charAt(el-1-i)))*Math.pow(10,i1));
            }
            i++;
            i1++;
        }
        while (j<sl){
            if(sd.charAt(sl-1-j)=='-'){
                j++;
                break;
            }
            else {
                num22= (int) (num22+Integer.parseInt(String.valueOf(sd.charAt(sl-1-j)))*Math.pow(10,j1));
            }
            j++;
            j1++;
        }
        if(num22>num11&&num2==num1){
            t=false;
            new AlertDialog.Builder(this)
                    .setTitle("Invalid Entry")
                    .setMessage("Ending date has to be greater than start date")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        int num111=0;
        int num222=0;
        int i2=0,j2=0;

        while (i<el){
            if(ed.charAt(el-1-i)=='/'){
                i++;
                break;
            }
            else{
                num111= (int) (num111+Integer.parseInt(String.valueOf(ed.charAt(el-1-i)))*Math.pow(10,i2));
            }
            i++;
            i2++;

        }
        while (j<sl){
            if(sd.charAt(sl-1-j)=='/'){
                j++;
                break;
            }
            else {
                num222= (int) (num222+Integer.parseInt(String.valueOf(sd.charAt(sl-1-j)))*Math.pow(10,j2));
            }
            j++;
            j2++;

        }
        if(num222>num111&&num2==num1){
            if(num222>num111&&num22==num11) {

                t = false;
                new AlertDialog.Builder(this)
                        .setTitle("Invalid Entry")
                        .setMessage("Ending date has to be greater than start date")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }

        if(t==true) {
            String mname = tname.getText().toString();
            sample.m_name = mname;

            Spinner meddose_ = (Spinner) findViewById(R.id.med_quantity);
            sample.m_quantity = meddose_.getSelectedItem().toString();

            String msd = tsd.getText().toString();
            sample.m_s_date = (msd);

            String mend = tend.getText().toString();
            sample.m_e_date = (mend);

            String temp = "";
            if (f == 1) {
                temp += "+";
            } else {
                temp += "-";
            }
            if (pob == 1) {
                temp += "+";
            } else {
                temp += "-";
            }
            if (prel == 1) {
                temp += "+";
            } else {
                temp += "-";
            }
            if (pol == 1) {
                temp += "+";
            } else {
                temp += "-";
            }
            if (pred == 1) {
                temp += "+";
            } else {
                temp += "-";
            }
            if (pod == 1) {
                temp += "+";
            } else {
                temp += "-";
            }
            if (n == 1) {
                temp += "+";
            } else {
                temp += "-";
            }
            String mtime = temp;
            sample.m_time = mtime;

            if (flag.equals("1")) {
                Firebase patient = root_ref.child(getIntent().getStringExtra("patid"));
                Firebase DatePrevious = patient.child(getIntent().getStringExtra("UpdateAtid"));
                DatePrevious.setValue(sample);
                Toast.makeText(this,"Data modified",Toast.LENGTH_LONG).show();
                finish();
            } else if (flag.equals("0")) {
                Firebase patient = root_ref.child(getIntent().getStringExtra("patid"));
                patient.push().setValue(sample);
                finish();
            }
        }
    }

    public void dell(View v){
        Firebase contact = root_ref.child(getIntent().getStringExtra("patid"));
        Firebase onDate = contact.child(getIntent().getStringExtra("UpdateToid"));
        onDate.removeValue();
        finish();
    }

    @Override
    protected Dialog onCreateDialog(int id){
        if(id==DIALOG_ID)
        {
            return new DatePickerDialog(this,dpickerListener,year_x,month_x,day_x);
        }
        else
            return null;
    }
    public String y,y1,y2;
    public String m,m1,m2;
    public String d,d1,d2;

    private DatePickerDialog.OnDateSetListener dpickerListener = new DatePickerDialog.OnDateSetListener(){
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            String day = "", month = "";
            d=String.valueOf(dayOfMonth);
            m=String.valueOf(monthOfYear+1);
            y=String.valueOf(year);
            if(dayOfMonth < 10) {
                day = "0";
            }
            if((monthOfYear+1) < 10){
                month =  "0";
            }
            String s= day+d+"-"+month+m+"-"+y;

            EditText date_of_r=(EditText)V1;
            date_of_r.setText(s, TextView.BufferType.EDITABLE);
        }
    };
    Calendar c = Calendar.getInstance();
    View V1;
    int mYear = c.get(Calendar.YEAR);
    int mMonth = c.get(Calendar.MONTH);
    int mDay = c.get(Calendar.DAY_OF_MONTH);

    public int year_x=mYear,month_x=mMonth,day_x=mDay;
    final int DIALOG_ID=0;

    public void showDialogg(View view){
        V1=findViewById(R.id.med_start);
        y1=y;
        m1=m;
        d1=d;
        showDialog(DIALOG_ID);

    }
    public void showDialoggg(View view){
        V1=findViewById(R.id.med_end);
        y2=y;
        m2=m;
        d2=d;
        showDialog(DIALOG_ID);

    }
}
