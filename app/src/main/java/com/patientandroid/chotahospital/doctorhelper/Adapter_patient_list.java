package com.patientandroid.chotahospital.doctorhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by varsh on 01-06-2016.
 */
public class Adapter_patient_list extends ArrayAdapter {

    List list = new ArrayList();

    public Adapter_patient_list(Context context, int resource) {
        super(context, resource);
    }

    static  class LayoutHandler{
        TextView name,age,gender;
    }

    public void add(Object object) {
        super.add(object);
        /**Adding each of the PatInfo content in list*/
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    /**Will return each row of data*/
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        /**Check if already the row is available or not*/
        LayoutHandler layoutHandler;
        if(row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.pat_row , parent , false);
            layoutHandler = new LayoutHandler();
            layoutHandler.name = (TextView) row.findViewById(R.id.name);
            layoutHandler.age = (TextView) row.findViewById(R.id.age);
            layoutHandler.gender = (TextView) row.findViewById(R.id.gender);
            row.setTag(layoutHandler);
        }
        else{
            layoutHandler = (LayoutHandler) row.getTag();
        }

        PatInfo patInfo = (PatInfo) this.getItem(position);
        layoutHandler.name.setText(patInfo.p_name);
        layoutHandler.age.setText(patInfo.p_age);
        layoutHandler.gender.setText(patInfo.p_gender);
        return row;
    }
}