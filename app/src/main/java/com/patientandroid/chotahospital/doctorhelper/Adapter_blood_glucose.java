package com.patientandroid.chotahospital.doctorhelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by varsh on 03-06-2016.
 */
public class Adapter_blood_glucose extends ArrayAdapter {
    List list = new ArrayList();

    public Adapter_blood_glucose(Context context, int resource) {
        super(context, resource);
    }

    static  class LayoutHandler{
        TextView value,type,date_of_starting;
    }

    public void add(Object object) {
        super.add(object);
        /**Adding each of the dataprovider content in list*/
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    /**Will return each row of data*/
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        /**Check if already the row is available or not*/
        LayoutHandler layoutHandler;
        if(row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.blood_glucose_row , parent , false);
            layoutHandler = new LayoutHandler();
            layoutHandler.value = (TextView) row.findViewById(R.id.text_value);
            layoutHandler.type = (TextView) row.findViewById(R.id.text_type);
            layoutHandler.date_of_starting = (TextView) row.findViewById(R.id.text_date_of_starting);
            row.setTag(layoutHandler);
        }
        else{
            layoutHandler = (LayoutHandler) row.getTag();
        }

        Glucose glucose = (Glucose) this.getItem(position);
        layoutHandler.value.setText(glucose.bg_value);
        layoutHandler.type.setText(glucose.bg_type);
        layoutHandler.date_of_starting.setText(glucose.bg_date);
        return row;
    }

}