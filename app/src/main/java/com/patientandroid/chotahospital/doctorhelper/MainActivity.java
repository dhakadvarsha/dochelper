package com.patientandroid.chotahospital.doctorhelper;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    SQLiteDatabase sqLiteDatabase;
    Adapter_patient_list adapter_patient_list;
    db_handler database;
    Context context = this;
    Cursor cursor;
    Integer place;
    private String p_id;

    Map<Integer,String> mapPositionToKey = new HashMap<>();
    Map<String,String> mapKeyToName = new HashMap<>();

    Firebase root_ref_patient , root_ref_doctor2patient;
    private static final String REFERENCE_URL_DOCTOR2PATIENT = "https://chotahospital-ffc48.firebaseio.com/Doc2PatientMap";
    private static final String REFERENCE_URL_PATEIENT = "https://chotahospital-ffc48.firebaseio.com/Patients";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        setTitle("My Patient");

        final FirebaseDatabase database = FirebaseDatabaseUtil.getDatabase();
        Firebase.setAndroidContext(this);
        root_ref_patient = new Firebase(REFERENCE_URL_PATEIENT);
        root_ref_doctor2patient = new Firebase(REFERENCE_URL_DOCTOR2PATIENT);


        listView = (ListView) findViewById(R.id.list_view_patient);
        registerForContextMenu(listView);

        adapter_patient_list = new Adapter_patient_list(getApplicationContext(), R.layout.pat_row);
        listView.setAdapter(adapter_patient_list);

        adapter_patient_list.clear();
        mapPositionToKey.clear();
        final DatabaseReference doctor = database.getReference("Doc2PatientMap/" + "919300646182");
//        Firebase doctor = root_ref_doctor2patient.child("919300646182");
        doctor.addValueEventListener(new com.google.firebase.database.ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {


                adapter_patient_list.clear();
                mapPositionToKey.clear();
                Log.e("Array List cleared","and map cleared");
                final Integer[] count = {0};

                for (com.google.firebase.database.DataSnapshot patinfo : dataSnapshot.getChildren()) {

                    String patid = String.valueOf(patinfo.getValue());
                    Log.e("patid",patid);

                    DatabaseReference patdetails = database.getReference("Patients/" + patid);
//                    Firebase patdetails = root_ref_patient.child(patid);

                    patdetails.addValueEventListener(new com.google.firebase.database.ValueEventListener() {
                        @Override
                        public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {

                                    Log.e("patient details",dataSnapshot.getKey());
                                    pat_info patinformation = dataSnapshot.getValue(pat_info.class);

                                    PatInfo patInfo = new PatInfo();
                                    patInfo.p_name = patinformation.getName();
                                    patInfo.p_gender = patinformation.getSex();
                                    patInfo.p_age = patinformation.getAge();
                                    /**Provide each of these to adapter for dispalying*/
                                    mapPositionToKey.put(count[0],dataSnapshot.getKey());
                                    ++count[0];
                                    mapKeyToName.put(dataSnapshot.getKey() ,patinformation.getName());
                                    adapter_patient_list.add(patInfo);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
                Log.e("Size of arrayList", String.valueOf(adapter_patient_list.getCount()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String patid = mapPositionToKey.get(position);

                Log.e("pateint key",patid);
                Log.e("patient Name" , mapKeyToName.get(patid));
                Intent i4 = new Intent(MainActivity.this, view_patient.class);
                i4.putExtra("Patient Name",mapKeyToName.get(patid));
                i4.putExtra("patid",patid);
                startActivity(i4);
                MainActivity.this.finish();



            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                p_id = mapPositionToKey.get(position);
                return false;
            }
        });
    }

    public void add_p(View view) {
        Intent intent = new Intent(this, add_patient.class);
        intent.putExtra("docId","919300646182");
        intent.putExtra("Update", 0);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add("Edit");
        menu.add("Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);

        if (item.getTitle() == "Edit") {

            Intent i = new Intent(MainActivity.this, add_patient.class);
            i.putExtra("Update", 1);
            i.putExtra("docId","919300646182");
            i.putExtra("patid",p_id);

            startActivity(i);

        } else if (item.getTitle() == "Delete") {

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setCancelable(true);
            builder.setTitle("Do you want to delete?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                    final Firebase doctor = root_ref_doctor2patient.child("919300646182");
                    doctor.child(p_id).removeValue();
                }

            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                }

            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_mainactivity, menu);

        TextView tv = new TextView(this);
        tv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, doc_profile.class);
                intent.putExtra("Profile Update", 1);
                // intent.putExtra("Name",user);
                startActivity(intent);
            }
        });

        tv.setTextColor(getResources().getColor(R.color.actionbar_title));
        tv.setPadding(5, 0, 5, 0);
        tv.setTextSize(18);
        tv.setText("Guest User");
        menu.add(0,1,0,"Username").setActionView(tv).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         int id = item.getItemId();
        if (id == R.id.editprofile){
        //Toast.makeText(this , "About to call Profile.class" , Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, doc_profile.class);
        intent.putExtra("Profile Update", 1);
        // intent.putExtra("Name",user);
        startActivity(intent);
        this.finish();
           }
         if (id == R.id.appointment){

             Calendar calendar = Calendar.getInstance();
             Intent intent = new Intent(this,appointmentSchedule.class);
             intent.putExtra("Go",0);
             intent.putExtra("Day",String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
             intent.putExtra("Month",String.valueOf(calendar.get(Calendar.MONTH)));
             intent.putExtra("Year",String.valueOf(calendar.get(Calendar.YEAR)));

             startActivity(intent);
             this.finish();
         }

        return super.onOptionsItemSelected(item);
    }

}