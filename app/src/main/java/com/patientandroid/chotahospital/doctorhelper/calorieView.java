package com.patientandroid.chotahospital.doctorhelper;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class calorieView extends Fragment {

    View inflatedview;
    String patientid ,date;
    TextView consumed , total;
    Double BurnedCalories = 0.0;

    Firebase root_ref_diet;
    private static final String REFERENCE_URL_DIET = "https://chotahospital-ffc48.firebaseio.com/Diet";

    public calorieView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        inflatedview =  inflater.inflate(R.layout.fragment_calorie_view, container, false);
        Firebase.setAndroidContext(getContext());//Initialises firebase for using this android app
        root_ref_diet = new Firebase(REFERENCE_URL_DIET);//Reference to data into firebase cloud.

        FirebaseDatabase database = FirebaseDatabaseUtil.getDatabase();

        consumed = (TextView) inflatedview.findViewById(R.id.consumed);
        total = (TextView) inflatedview.findViewById(R.id.total);

        /**
         * to calculate the total calories consumed in a day by a person
         */
        DatabaseReference Date = database.getReference("Diet/" + patientid +"/"+date);

        Date.addValueEventListener(new com.google.firebase.database.ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                BurnedCalories = 0.0;
                if(dataSnapshot.exists()){

                    for (com.google.firebase.database.DataSnapshot  key : dataSnapshot.getChildren()) {
                        {
                            for (com.google.firebase.database.DataSnapshot Reading : key.getChildren()) {
                                Log.e("Reading is",Reading.getKey());

                                storeInfo storeinfo = Reading.getValue(storeInfo.class);
                                BurnedCalories = BurnedCalories + Double.parseDouble(storeinfo.caloriesConsumed);
                            }
                        }

                    }

                }
                consumed.setText(String.valueOf(BurnedCalories) + " calories");

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return  inflatedview;
    }

    //sets the person's id whose calories are to be calculated
    public void SetPatientid(String personid){
        this.patientid = personid;
    }

    //set Date for which total calories are calculated
    public void SetDate(String date){
        this.date = date;
    }


}
