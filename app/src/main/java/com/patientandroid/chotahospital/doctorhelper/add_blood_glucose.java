package com.patientandroid.chotahospital.doctorhelper;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class add_blood_glucose extends AppCompatActivity {

    EditText value,date_of_starting,comments;
    Context context = this;
    db_handler database;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;
    String item,contact_no;
    Integer i=0,k=0;

    Firebase root_ref;
    private static final String REFERENCE_URL = "https://chotahospital-ffc48.firebaseio.com/Patients";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_blood_glucose);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Add Record");

        Firebase.setAndroidContext(this);//Initialises firebase for using this android app
        root_ref = new Firebase(REFERENCE_URL);//Reference to data into firebase cloud.

        contact_no = getIntent().getStringExtra("Contact_No");

        value = (EditText) findViewById(R.id.editText);
        date_of_starting = (EditText) findViewById(R.id.editText2);
        comments = (EditText) findViewById(R.id.editText3);

        Spinner spinner = (Spinner) findViewById(R.id.editText4);

        i = getIntent().getIntExtra("Value_id",0);

        final Integer temp = 0;
        final String[] DateSelected = {""};
        final String[] TypeBg = {""};
        if(getIntent().getStringExtra("DateUpdate") != null){
            Firebase contact = root_ref.child(contact_no);
            Firebase patient = contact.child("BloodGlucose");
            Firebase onDateUpdate = patient.child(getIntent().getStringExtra("DateUpdate"));
            final String id = getIntent().getStringExtra("Positionbg");

            onDateUpdate.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for (DataSnapshot Reading : dataSnapshot.getChildren()) {
                        if(id != null){
                            if(Reading.getKey().equals(id)){

                                DateSelected[0] = Reading.getKey();
                                Log.e("Read is",Reading.getKey());
                                Glucose ReadData = Reading.getValue(Glucose.class);
                                value.setText(ReadData.getBg_value());
                                date_of_starting.setText(getIntent().getStringExtra("DateUpdate"));
                                comments.setText(ReadData.getBg_comments());
                                TypeBg[0] = ReadData.getBg_type();
                                Log.e("type bg",ReadData.getBg_type());
                                break;
                            }
                        }
                        if(!(DateSelected[0].equals(""))){
                            break;
                        }
                    }


                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }

        if(i == 1){
            //Update the record
            setTitle("Update Record");

            String [] dosages = {"Fasting","Post-BreakFast","Pre-Lunch","Post-Lunch","Pre-Dinner","Post-Dinner","Night"};
            for(int j=0;j<dosages.length;j++){
                if (TypeBg[0].equals(dosages[j])){
                    k=j;
                }
            }

        }

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.type_name, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(k);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void add_info(View view){

        Glucose glucose = new Glucose();

        glucose.bg_value = value.getText().toString();
        glucose.bg_type = item;
        String dateOfStart = date_of_starting.getText().toString();
        glucose.bg_comments = comments.getText().toString();

        if(glucose.bg_value.length() == 0){
            Toast.makeText(add_blood_glucose.this , "Please fill the Blood Glucose level field" , Toast.LENGTH_LONG).show();
        }
        else if(dateOfStart.equals("")){
            Toast.makeText(add_blood_glucose.this , "Please fill the Date field" , Toast.LENGTH_LONG).show();
        }
        else if(i==1){

            Firebase patient = root_ref.child(contact_no);
            Firebase bgData = patient.child("BloodGlucose");
            Firebase DatePrevious = bgData.child(getIntent().getStringExtra("DateUpdate"));
            Firebase DateData = bgData.child(date_of_starting.getText().toString());
            Firebase idRef = DateData.child(getIntent().getStringExtra("Positionbg"));

            if(!((date_of_starting.getText().toString()).equals(getIntent().getStringExtra("DateUpdate")))){
                DatePrevious.child(getIntent().getStringExtra("Positionbg")).removeValue();
                Log.e("Remove",getIntent().getStringExtra("Positionbg"));
            }
            idRef.setValue(glucose);

            Toast.makeText(getBaseContext(),"Data modified",Toast.LENGTH_LONG).show();

            Intent intent = new Intent(this , view_blood_glucose.class);
            intent.putExtra("Contact_No",contact_no);
            intent.putExtra("Position",getIntent().getIntExtra("Position",0));
            startActivity(intent);
            this.finish();


        }
        else{

            Firebase patient = root_ref.child(contact_no);
            Firebase bgData = patient.child("BloodGlucose");
            Firebase DateData = bgData.child(date_of_starting.getText().toString());


            DateData.push().setValue(glucose);

            Intent intent = new Intent(this , view_blood_glucose.class);
            intent.putExtra("Contact_No",contact_no);
            intent.putExtra("Position",getIntent().getIntExtra("Position",0));
            startActivity(intent);
            this.finish();
        }
    }

    Calendar myCalendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    public void Datepicker(View view){
        new DatePickerDialog(this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLabel() {

        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        date_of_starting.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_blood_glucose, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent(this, view_blood_glucose.class);
            intent.putExtra("Contact_No",getIntent().getStringExtra("Contact_No"));
            intent.putExtra("Position",getIntent().getIntExtra("Position",0));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this, view_blood_glucose.class);
        intent.putExtra("Contact_No",getIntent().getStringExtra("Contact_No"));
        intent.putExtra("Position",getIntent().getIntExtra("Position",0));
        startActivity(intent);
        finish();

    }
}
