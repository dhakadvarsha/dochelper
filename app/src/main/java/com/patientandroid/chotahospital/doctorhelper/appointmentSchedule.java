package com.patientandroid.chotahospital.doctorhelper;


import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.alamkanak.weekview.WeekViewLoader;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import hirondelle.date4j.DateTime;

public class appointmentSchedule extends AppCompatActivity{

    Context context = this;
    String date1;
    WeekView mWeekView;
    Integer i=0,setdate = 0;
    String Slot[];
    Integer appointmentCounter[];
    List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();
    int check = 0;

    private static final Integer past_month = 3;
    private static final Integer future_month = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_schedule);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Appointments");


        mWeekView = (WeekView) findViewById(R.id.weekView);
        mWeekView.setNumberOfVisibleDays(1);
        mWeekView.goToHour(8.00);
        mWeekView.setHorizontalScrollBarEnabled(false);
        mWeekView.setEventTextColor(getResources().getColor(R.color.black));
        mWeekView.setHourHeight(200);
        mWeekView.setXScrollingSpeed(1);

        final CaldroidFragment caldroidFragment = new CaldroidFragment();
        setdate = getIntent().getIntExtra("Go",0);

        date1 = getIntent().getStringExtra("Day") + "-" + getIntent().getStringExtra("Month") + "-" + getIntent().getStringExtra("Year");
        Log.e("Month is",String.valueOf(getIntent().getStringExtra("Month")));
        Log.e("Year is",String.valueOf(getIntent().getStringExtra("Year")));

        Calendar calendar = Calendar.getInstance();

        /**
         * For setting the date and time after returning from an actvity.
         */
        if(setdate == 1){

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            /**
             * String timeslot = getIntent().getStringExtra("Time-Slot");
             String[] parts = timeslot.split("-");
             timeslot = parts[1];
             parts = timeslot.split(":");

             Log.e("StartHour",parts[0].replaceAll("\\s",""));

             int starthour = Integer.parseInt(parts[0].replaceAll("\\s",""));
             parts = (parts[0]).split(" ");
             if(parts[1].equals("PM")){
             if(starthour != 12){
             starthour = 12 + starthour;
             mWeekView.goToHour(starthour);
             }
             }

             */
            Date highlight = new Date();
            try {
                highlight = sdf.parse(date1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            calendar.setTime(highlight);

            //Go to specific date in WeekView
            int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
            calendar.set(Calendar.DAY_OF_WEEK,weekDay);
            calendar.set(Calendar.DAY_OF_MONTH,Integer.parseInt(getIntent().getStringExtra("Day")));
            calendar.set(Calendar.MONTH , Integer.parseInt(getIntent().getStringExtra("Month"))-1);
            calendar.set(Calendar.YEAR,Integer.parseInt(getIntent().getStringExtra("Year")));
            mWeekView.goToDate(calendar);

            //set the date in caldroid.
            Bundle args = new Bundle();
            args.putInt(CaldroidFragment.MONTH, Integer.parseInt(getIntent().getStringExtra("Month")));
            args.putInt(CaldroidFragment.YEAR, Integer.parseInt(getIntent().getStringExtra("Year")));
            args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, false);
            caldroidFragment.setArguments(args);
            caldroidFragment.setTextColorForDate(R.color.red,highlight);

        }

        else {
            Bundle args = new Bundle();
            args.putInt(CaldroidFragment.MONTH, calendar.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, calendar.get(Calendar.YEAR));
            args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, false);
            caldroidFragment.setArguments(args);
        }

        /**
         * Listener for calender , basically a selected date listener.
         */

        caldroidFragment.setCaldroidListener(new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {

                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String date2 = sdf.format(date);

                String[] parts = date2.split("-");
                final String year1 = parts[2], month1 = parts[1], day1 = parts[0];

                Date highlight = new Date();
                try {
                    highlight = sdf.parse(date2);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                calendar.setTime(highlight);

                int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
                calendar.set(Calendar.DAY_OF_WEEK, weekDay);
                calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day1));
                calendar.set(Calendar.MONTH, Integer.parseInt(month1) - 1);
                calendar.set(Calendar.YEAR, Integer.parseInt(year1));
                Log.e("Go to Date", calendar.toString());

                Intent intent = new Intent(appointmentSchedule.this , appointmentSchedule.class);
                intent.putExtra("Go",1);
                intent.putExtra("Day",day1);
                intent.putExtra("Month",month1);
                intent.putExtra("Year",year1);

                startActivity(intent);
                appointmentSchedule.this.finish();
            }

        });

        /**
         * Month listener for week View , set the events for the week view
         */

        mWeekView.setMonthChangeListener(new MonthLoader.MonthChangeListener() {
            @Override
            public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {

                List<WeekViewEvent> e = new ArrayList<WeekViewEvent>();

                if(check == 0){
                    check = 1;
                    return  getEvents();
                }
                else
                    return e;
            }

        });

        /**
         * To set the appointments by clicking on the time-slot on the week view
         */
        mWeekView.setEmptyViewClickListener(new WeekView.EmptyViewClickListener() {
            @Override
            public void onEmptyViewClicked(Calendar time) {

                int startmin=0,endhour=0,starthour=0,endmin=0;

                int selectedHour = time.getTime().getHours();
                int selectedMinute = time.getTime().getMinutes();

                String x = time.getTime().toString();
                String[] part = x.split(" ");

                int selectedDay = time.getTime().getDate();
                int selectedMonth = time.getTime().getMonth() + 1;
                int selecteddYear = Integer.parseInt(part[5]);

                Date da = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(da);
                cal.add(Calendar.DAY_OF_MONTH, -1);
                da = cal.getTime();

                if((time.getTime()).before(da)){
                    Toast.makeText(appointmentSchedule.this , "Schedule cannot be modified", Toast.LENGTH_LONG).show();
                }
                else if(selectedHour >=8 && selectedHour<21){

                    if(selectedMinute>=0 && selectedMinute <30){
                        startmin=0;
                        endmin=30;
                        starthour = selectedHour;
                        endhour = selectedHour;
                    }
                    else{
                        startmin = 30;
                        endmin = 0;
                        starthour = selectedHour;
                        endhour = selectedHour + 1;
                    }

                    String temp1 = String.valueOf(selectedDay),temp2 = String.valueOf(selectedMonth);

                    if(selectedDay < 10){
                        temp1 = "0"+selectedDay;
                    }
                    if(selectedMonth < 10){
                        temp2 = "0"+selectedMonth;
                    }

                    String selectedDate = temp1+"-"+temp2+"-"+selecteddYear;

                    int slots = starthour;
                    int slote = endhour;
                    String ts = "AM";
                    String te = "AM";
                    if(starthour > 12){
                        slots = starthour - 12;
                        ts = "PM";
                    }
                    else if(selectedHour == 12){
                        ts = "PM";
                    }

                    if(endhour > 12){
                        slote = starthour - 12;
                        te = "PM";
                    }
                    else if(selectedHour == 12){
                        te = "PM";
                    }

                    String sm = String.valueOf(startmin);
                    String em = String.valueOf(endmin);

                    if(startmin == 0){
                        sm = "00";
                    }
                    if(endmin == 0){
                        em = "00";
                    }
                    String temp = String.valueOf(slots)+":"+sm+" "+ts+" - "+String.valueOf(slote)+":"+em+" "+te;


                    /**
                     * Log.e("Date-Month-Year",selectedDay+" "+selectedMonth+" "+selecteddYear);
                     Calendar startTime = Calendar.getInstance();
                     startTime.set(Calendar.HOUR_OF_DAY, starthour);
                     startTime.set(Calendar.MINUTE, startmin);
                     startTime.set(Calendar.MONTH, selectedMonth-1);
                     startTime.set(Calendar.YEAR, selecteddYear);
                     startTime.set(Calendar.DAY_OF_MONTH,selectedDay);
                     Calendar endTime = (Calendar) startTime.clone();
                     endTime.set(Calendar.HOUR_OF_DAY, endhour);
                     endTime.set(Calendar.MINUTE, endmin);
                     WeekViewEvent event = new WeekViewEvent(i, "2 Appointments Available", startTime, endTime);
                     event.setColor(getResources().getColor(R.color.yellow));
                     events.add(event);
                     */

                    Log.e("Date...",selectedDate);
                    Intent intent = new Intent(appointmentSchedule.this , addapointment.class);
                    intent.putExtra("Date" , selectedDate);
                    intent.putExtra("Time-Slot",temp);
                    startActivity(intent);
                    appointmentSchedule.this.finish();

                }
                else{
                    Toast.makeText(appointmentSchedule.this , "Selected time is invalid", Toast.LENGTH_LONG).show();
                }



            }
        });

        /**
         * For adding more appointments to already set event.
         */

        mWeekView.setOnEventClickListener(new WeekView.EventClickListener() {
            @Override
            public void onEventClick(WeekViewEvent event, RectF eventRect) {
                long id = event.getId();
                String TimeSlot = Slot[Integer.parseInt(String.valueOf(id))];
                final String temp = TimeSlot;
                if(event.getColor() == getResources().getColor(R.color.red)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(appointmentSchedule.this);
                    builder.setCancelable(true);
                    builder.setTitle("Bookings are closed for this slot , schedule anyway??");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent = new Intent(appointmentSchedule.this , addapointment.class);
                            intent.putExtra("Date" , date1);
                            intent.putExtra("Time-Slot",temp);
                            startActivity(intent);
                            appointmentSchedule.this.finish();

                        }

                    });

                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {

                        }

                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                else{
                    Intent intent = new Intent(appointmentSchedule.this , addapointment.class);
                    intent.putExtra("Date" , date1);
                    intent.putExtra("Time-Slot",TimeSlot);
                    startActivity(intent);
                    appointmentSchedule.this.finish();
                }
            }
        });

        android.support.v4.app.FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_appointmentschedule, menu);
        return  true;
    }

    /**
     *Select the option from menu bar
     * id Goto gor taking the user at today's date and id availablity for check availablity on particular date and time slot.
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();

        }
        if (id == R.id.Goto){



            Calendar calendar = Calendar.getInstance();
            Intent intent = new Intent(this,appointmentSchedule.class);
            intent.putExtra("Go",0);
            intent.putExtra("Day",String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
            intent.putExtra("Month",String.valueOf(calendar.get(Calendar.MONTH)));
            intent.putExtra("Year",String.valueOf(calendar.get(Calendar.YEAR)));

            startActivity(intent);
            this.finish();

        }
        if (id == R.id.availablity){
            Intent intent = new Intent(this,availablity_check.class);
            startActivity(intent);
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Takes to mainactivity when the back button is pressed
     */
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();

    }

    /**
     *extract the events from database for setting them to week view
     * @return events to be shown on week view.
     */

    public List<WeekViewEvent> getEvents(){


        InputStream is = null;
        try {
            is = new FileInputStream(Environment.getExternalStorageDirectory() + File.separator +
                    "Android" + File.separator + "data" + File.separator + "com.chotahospital.appointment" + File.separator + "event.json");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        InputStream stream = is;
        BufferedReader reader = null;
        reader = new BufferedReader(new InputStreamReader(stream));
        StringBuffer buffer = new StringBuffer();
        String line = "";
        try {
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(buffer.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Slot = new String[]{"8:00 AM - 8:30 AM", "8:30 AM - 9:00 AM", "9:00 AM - 9:30 AM", "9:30 AM - 10:00 AM",
                "10:00 AM - 10:30 AM", "10:30 AM - 11:00 AM", "11:00 AM - 11:30 AM", "11:30 AM - 12:00 PM",
                "12:00 PM - 12:30 PM", "12:30 PM - 1:00 PM", "1:00 PM - 1:30 PM", "1:30 PM - 2:00 PM",
                "2:00 PM - 2:30 PM", "2:30 PM - 3:00 PM", "3:00 PM - 3:30 PM", "3:30 PM - 4:00 PM", "4:00 PM - 4:30 PM",
                "4:30 PM - 5:00 PM", "5:00 PM - 5:30 PM", "5:30 PM - 6:00 PM", "6:00 PM - 6:30 PM", "6:30 PM - 7:00 PM",
                "7:00 PM - 7:30 PM", "7:30 PM - 8:00 PM", "8:00 PM - 8:30 PM", "8:30 PM - 9:00 PM"};

        try {

            Log.e("No Exception", "");
            Iterator<String> keys = jsonObject != null ? jsonObject.keys() : null;
            ArrayList<String> allDates = new ArrayList<>();

            if (keys != null) {
                while (keys.hasNext()){
                    String key = keys.next();
                    allDates.add(key);//all the dates from database is extracted.
                }
            }

            for(int k=0;k<allDates.size();++k){

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                Date highlight = new Date();
                try {
                    highlight = sdf.parse(allDates.get(k));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MONTH, -(past_month));
                Date result = cal.getTime();

                if(new Date().compareTo(highlight) <= future_month || highlight.after(result)){

                    appointmentCounter = new Integer[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
                    String[] part = (allDates.get(k)).split("-");

                    int newDate = Integer.parseInt(part[0]);
                    int newMonth = Integer.parseInt(part[1]);
                    int newYear = Integer.parseInt(part[2]) ;

                    Log.e("Date-Month-Year",part[0]+" "+part[1]+" "+part[2]);

                    if (jsonObject != null && jsonObject.has(allDates.get(k))) {
                        JSONArray array = jsonObject.getJSONArray(allDates.get(k));
                        for (int j = 0; j < Slot.length; ++j) {
                            Integer temp = 0;
                            for (int i = 0; i < array.length(); ++i) {
                                if ((array.getJSONObject(i).get("Time")).toString().equals(Slot[j])) {
                                    ++temp;
                                }
                            }
                            appointmentCounter[j] = temp;
                            //Log.e("Appointment is ",String.valueOf(temp));

                        }
                    }

                    Integer starthour = 7, endhour = 8;

                    for (int i = 0; i < Slot.length; ++i) {
                        Integer startmin = 0, endmin = 0;

                        if (i % 2 == 0) {
                            starthour = starthour + 1;
                            endmin = 30;
                        } else {
                            startmin = 30;
                            endhour = endhour + 1;
                        }

                        //Log.e("Now " , String.valueOf(appointmentCounter[i]));
                        //Log.e("About to enter " , "");
                        if(appointmentCounter[i] == 1){

                            //Log.e("entered " , "");
                            Calendar startTime = Calendar.getInstance();
                            startTime.set(Calendar.HOUR_OF_DAY, starthour);
                            startTime.set(Calendar.MINUTE, startmin);
                            startTime.set(Calendar.MONTH, newMonth-1);
                            startTime.set(Calendar.YEAR, newYear);
                            startTime.set(Calendar.DAY_OF_MONTH,newDate);
                            Calendar endTime = (Calendar) startTime.clone();
                            endTime.set(Calendar.HOUR_OF_DAY, endhour);
                            endTime.set(Calendar.MINUTE, endmin);
                            WeekViewEvent event = new WeekViewEvent(i, "2 Appointments Available",startTime,endTime);
                            event.setColor(getResources().getColor(R.color.yellow));
                            events.add(event);
                            Log.e("Item added tp event","");

                        }

                        else if(appointmentCounter[i] == 2){

                            Calendar startTime = Calendar.getInstance();
                            startTime.set(Calendar.HOUR_OF_DAY, starthour);
                            startTime.set(Calendar.MINUTE, startmin);
                            startTime.set(Calendar.MONTH, newMonth-1);
                            startTime.set(Calendar.YEAR, newYear);
                            startTime.set(Calendar.DAY_OF_MONTH,newDate);
                            Calendar endTime = (Calendar) startTime.clone();
                            endTime.set(Calendar.HOUR_OF_DAY, endhour);
                            endTime.set(Calendar.MINUTE, endmin);
                            WeekViewEvent event = new WeekViewEvent(i, "1 Appointment Available",startTime,endTime);

                            event.setColor(getResources().getColor(R.color.orange));
                            events.add(event);
                            Log.e("Item added tp event","");

                        }

                        else if(appointmentCounter[i] >= 3){

                            Calendar startTime = Calendar.getInstance();
                            startTime.set(Calendar.HOUR_OF_DAY, starthour);
                            startTime.set(Calendar.MINUTE, startmin);
                            startTime.set(Calendar.MONTH, newMonth-1);
                            startTime.set(Calendar.YEAR, newYear);
                            startTime.set(Calendar.DAY_OF_MONTH,newDate);
                            Calendar endTime = (Calendar) startTime.clone();
                            endTime.set(Calendar.HOUR_OF_DAY, endhour);
                            endTime.set(Calendar.MINUTE, endmin);
                            WeekViewEvent event = new WeekViewEvent(i, "No Appointment Available",startTime,endTime);
                            event.setColor(getResources().getColor(R.color.red));
                            events.add(event);
                            Log.e("Item added tp event","");
                        }
                    }

                }

            }
        }catch (JSONException e) {
            Log.e("Exception", "");
            e.printStackTrace();
        }
        return events;
    }

}
