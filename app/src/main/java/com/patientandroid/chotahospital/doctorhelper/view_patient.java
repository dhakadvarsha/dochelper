package com.patientandroid.chotahospital.doctorhelper;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class view_patient extends AppCompatActivity {

    String contact_no;
    Context context = this;
    db_handler database;
    SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_patient);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        contact_no = getIntent().getStringExtra("Contact_No");
        setTitle("Report");

    }

    public void pat_blood_glucose(View v){
        Intent i=new Intent(this,view_blood_glucose.class);
        // Toast.makeText(this , "contact no received from mainactivity is "+contact_no,Toast.LENGTH_SHORT).show();
        i.putExtra("Contact_No",contact_no);
        i.putExtra("Position",getIntent().getIntExtra("Position",0));
        startActivity(i);
        this.finish();
    }

    public void pat_insulin(View v){
        Intent i=new Intent(this,view_insulin.class);
        i.putExtra("Contact_No",contact_no);
        i.putExtra("Position",getIntent().getIntExtra("Position",0));
        startActivity(i);
        this.finish();
    }

    public void pat_medication(View v){
        //String ss=getIntent().getStringExtra("Contact_No");
        Intent i=new Intent(this,view_medication.class);
        i.putExtra("patid",getIntent().getStringExtra("patid"));
        i.putExtra("Patient Name",getIntent().getStringExtra("Patient Name"));
        startActivity(i);
    }

    public void view_graph(View v){
        Intent i=new Intent(this,view_blood_glucose_graph.class);
        i.putExtra("Contact_No",contact_no);
        i.putExtra("Position",getIntent().getIntExtra("Position",0));
        startActivity(i);
        this.finish();
    }

    public void diet(View v){
        Intent i=new Intent(this,view_diet.class);
        i.putExtra("patid",getIntent().getStringExtra("patid"));
        i.putExtra("Patient Name",getIntent().getStringExtra("Patient Name"));
        startActivity(i);
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_view_patient, menu);
        database = new db_handler(this);
        sqLiteDatabase = database.getReadableDatabase();
        Cursor cursor = database.getinfo_patient(sqLiteDatabase);
        cursor.moveToPosition(getIntent().getIntExtra("Position",0));
        TextView tv = new TextView(this);

        //Write Patient name of sufficient lenght
        Log.e("patientName in view",getIntent().getStringExtra("Patient Name"));
        String name = getIntent().getStringExtra("Patient Name");
        String[] part = name.split(" ");
        if((name.substring(0,(part[0]).length()).length() > 10)){
            tv.setText(name.substring(0,10));
        }
        else{
            tv.setText(part[0]);
        }
        tv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view_patient.this, add_patient.class);
                intent.putExtra("Update", 1);
                intent.putExtra("Position",getIntent().getIntExtra("Position",0));
                startActivity(intent);
                view_patient.this.finish();
            }
        });
        tv.setTextColor(getResources().getColor(R.color.actionbar_title));
        tv.setPadding(5, 0, 5, 0);
        tv.setTextSize(18);
        menu.add(0,1,0,"Username").setActionView(tv).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        database.close();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.editpatient_info){
            //Toast.makeText(this , "About to call Profile.class" , Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, add_patient.class);
            intent.putExtra("Update", 1);
            intent.putExtra("Position",getIntent().getIntExtra("Position",0));
            startActivity(intent);
            this.finish();

        }

        if(id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            view_patient.this.finish();
            return true;
        }

        if(id == R.id.deletepatient_info){

            database = new db_handler(context);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Do you want to delete?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    database.deletePatInfo(getIntent().getStringExtra("Contact_No"));
                    Toast.makeText(view_patient.this, "Data Deleted", Toast.LENGTH_LONG).show();

                    Intent i2 = new Intent(view_patient.this, MainActivity.class);
                    startActivity(i2);
                    view_patient.this.finish();

                    database.close();
                }

            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                }

            });
            AlertDialog alert = builder.create();
            alert.show();

        }
        return super.onOptionsItemSelected(item);
    }


}
