package com.patientandroid.chotahospital.doctorhelper;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class doc_profile extends AppCompatActivity {


    db_handler database;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;
    String g="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doc_profile);

        database = new db_handler(this,null,null,1);
        sqLiteDatabase = database.getReadableDatabase();
        cursor = db_handler.get_doc_profile(sqLiteDatabase);
        DocInfo temp=new DocInfo();

        if(!cursor.isAfterLast())
            if (cursor.moveToLast()) {

                do {
                    DocInfo temp2=new DocInfo();
                    temp2.d_contact_no = cursor.getString(0);
                    temp2.d_name = cursor.getString(1);
                    temp2.d_sex = cursor.getString(2);
                    temp2.d_speciality =cursor.getString(3);
                    temp2.d_email_id =cursor.getString(4);
                    temp2.d_age = cursor.getString(5);
                    temp2.d_work_exp = cursor.getString(6);
                    temp=temp2;
                    g+=temp2.d_contact_no;
                } while (cursor.moveToPrevious());
            }
        int a=0;
        if(temp.d_contact_no != null){
            a=temp.d_contact_no.length();
        }
        if(a>0)
        {
            EditText tname=(EditText) (findViewById(R.id.name));
            tname.setText(temp.d_name, TextView.BufferType.EDITABLE);

            EditText tsex=(EditText)findViewById(R.id.sex);
            tsex.setText(temp.d_sex, TextView.BufferType.EDITABLE);

            EditText tcontact=(EditText)findViewById(R.id.contact_no);
            tcontact.setText(temp.d_contact_no, TextView.BufferType.EDITABLE);

            EditText tspecial=(EditText)findViewById(R.id.speciality);
            tspecial.setText(temp.d_speciality, TextView.BufferType.EDITABLE);

            EditText temail=(EditText)findViewById(R.id.email);
            temail.setText(temp.d_email_id, TextView.BufferType.EDITABLE);

            TextView tage=(TextView)findViewById(R.id.age);
            tage.setText(temp.d_age, TextView.BufferType.EDITABLE);

            TextView twork=(TextView)findViewById(R.id.working_exp);
            twork.setText(temp.d_work_exp, TextView.BufferType.EDITABLE);

        }

    }

    public void submit(View v){
        DocInfo sample=new DocInfo();

        TextView tname=(TextView)(findViewById(R.id.name));
        String name=tname.getText().toString();
        sample.d_name=(name);

        TextView tsex=(TextView)findViewById(R.id.sex);
        String sex=tsex.getText().toString();
        sample.d_sex=(sex);

        TextView tcontact=(TextView)findViewById(R.id.contact_no);
        String contact=tcontact.getText().toString();
        sample.d_contact_no=(contact);

        TextView tspecial=(TextView)findViewById(R.id.speciality);
        String special=tspecial.getText().toString();
        sample.d_speciality=(special);

        TextView temail=(TextView)findViewById(R.id.email);
        String email=temail.getText().toString();
        sample.d_email_id=email;

        TextView tage=(TextView)findViewById(R.id.age);
        String age=tage.getText().toString();
        sample.d_age=age;

        TextView twork=(TextView)findViewById(R.id.working_exp);
        String work=twork.getText().toString();
        sample.d_work_exp=work;

        db_handler ob=new db_handler(this,null,null,1);
        ob.addDocInfo(sample,g);
        finish();
    }
}
