package com.patientandroid.chotahospital.doctorhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by varsh on 12-06-2016.
 */
public class Adapter_appointment extends ArrayAdapter {

    List list = new ArrayList();

    public Adapter_appointment(Context context, int resource) {
        super(context, resource);
    }

    static  class LayoutHandler{
        TextView name , mobilenumber , hour , minute , age , gender;
    }

    public void add(Object object) {
        super.add(object);
        /**Adding each of the dataprovider content in list*/
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    /**Will return each row of data*/
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        /**Check if already the row is available or not*/
        LayoutHandler layoutHandler;
        if(row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.appointment_row , parent , false);
            layoutHandler = new LayoutHandler();
            layoutHandler.name = (TextView) row.findViewById(R.id.row1);
            //layoutHandler.mobilenumber = (TextView) row.findViewById(R.id.row2);
            //layoutHandler.hour = (TextView) row.findViewById(R.id.row3);
            //layoutHandler.minute = (TextView) row.findViewById(R.id.row3b);
            layoutHandler.age = (TextView) row.findViewById(R.id.row2);
            //layoutHandler.gender = (TextView) row.findViewById(R.id.row4b);
            row.setTag(layoutHandler);
        }
        else{
            layoutHandler = (LayoutHandler) row.getTag();
        }
        pat_appointment appointment = (pat_appointment) this.getItem(position);
        layoutHandler.name.setText(appointment.pat_name);
        //layoutHandler.mobilenumber.setText(appointment.pat_mob);
        //layoutHandler.hour.setText(appointment.pat_appointment_hour);
        //layoutHandler.minute.setText(appointment.pat_appointment_minute);
        layoutHandler.age.setText(appointment.pat_age);
        //layoutHandler.gender.setText(appointment.pat_gender);
        return row;
    }
}
