package com.patientandroid.chotahospital.doctorhelper;



import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class view_blood_glucose_graph extends AppCompatActivity{

    //ActionBar actionBar;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_blood_glucose_graph);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Graphs");

        tabLayout = (TabLayout) findViewById(R.id.fasting_tab);
        viewPager = (ViewPager) findViewById(R.id.viewpager_fasting);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        /*Bundle bundle = new Bundle();
        bundle.putString("Contact_No", getIntent().getStringExtra("Contact_No"));
        */

        HomeFragment homeFragment = new HomeFragment();
        FirstFragment firstFragment = new FirstFragment();
        SecondFragment secondFragment = new SecondFragment();
        PostLunch postLunch = new PostLunch();
        PreDinner preDinner = new PreDinner();
        PostDinner postDinner = new PostDinner();
        Night night = new Night();

        /*homeFragment.setArguments(bundle);
        firstFragment.setArguments(bundle);
        secondFragment.setArguments(bundle);
        postLunch.setArguments(bundle);
        preDinner.setArguments(bundle);
        postDinner.setArguments(bundle);
        night.setArguments(bundle);
        */


        homeFragment.SetContact(getIntent().getStringExtra("Contact_No"));
        firstFragment.SetContact(getIntent().getStringExtra("Contact_No"));
        secondFragment.SetContact(getIntent().getStringExtra("Contact_No"));
        postLunch.SetContact(getIntent().getStringExtra("Contact_No"));
        preDinner.SetContact(getIntent().getStringExtra("Contact_No"));
        postDinner.SetContact(getIntent().getStringExtra("Contact_No"));
        night.SetContact(getIntent().getStringExtra("Contact_No"));

        viewPagerAdapter.add(homeFragment, "Fasting");
        viewPagerAdapter.add(firstFragment, "Post-Breakfast");
        viewPagerAdapter.add(secondFragment, "Pre-Lunch");
        viewPagerAdapter.add(postLunch, "Post-Lunch");
        viewPagerAdapter.add(preDinner, "Pre-Dinner");
        viewPagerAdapter.add(postDinner, "Post-Dinner");
        viewPagerAdapter.add(night, "Night");

        viewPager.setOffscreenPageLimit(0);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_view_blood_glucose, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent(this, view_patient.class);
            intent.putExtra("Contact_No",getIntent().getStringExtra("Contact_No"));
            intent.putExtra("Patient Name",getIntent().getStringExtra("Patient Name"));
            intent.putExtra("Position",getIntent().getIntExtra("Position",0));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
}