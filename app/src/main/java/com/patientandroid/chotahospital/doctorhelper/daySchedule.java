package com.patientandroid.chotahospital.doctorhelper;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class daySchedule extends AppCompatActivity {

    String date;
    TabLayout tabLayout;
    ViewPager viewPager;
    ScheduleAdapter scheduleAdapter;
    Integer fragment_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_schedule);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        date = getIntent().getStringExtra("Date");
        setTitle(date);

        tabLayout = (TabLayout) findViewById(R.id.tab);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);//Force Tabs to occpy complete width.
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        scheduleAdapter = new ScheduleAdapter(getSupportFragmentManager());

        Morning morning = new Morning();
        Afternoon afternoon = new Afternoon();
        Evening evening = new Evening();

        scheduleAdapter.add(morning,"Morning");
        scheduleAdapter.add(afternoon,"Afternoon");
        scheduleAdapter.add(evening,"Evening");

        viewPager.setOffscreenPageLimit(0);
        viewPager.setAdapter(scheduleAdapter);
        tabLayout.setupWithViewPager(viewPager);

        Calendar c = Calendar.getInstance();
        Integer hour = c.get(Calendar.HOUR_OF_DAY);

        Bundle bundle = new Bundle();
        bundle.putString("Date",date);

        morning.setArguments(bundle);
        afternoon.setArguments(bundle);
        evening.setArguments(bundle);

        Log.e("hour" , hour.toString());

        if(hour < 12){
            viewPager.setCurrentItem(0);
            fragment_type = 0;
        }
        else  if(hour >= 12 && hour < 17){
            viewPager.setCurrentItem(1);
            fragment_type = 1;
        }
        else {
            viewPager.setCurrentItem(2);
            fragment_type = 2;
        }
    }


}
