package com.patientandroid.chotahospital.doctorhelper;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class add_patient extends AppCompatActivity {

    EditText contact_no,name , height , weight , age;
    String gender,old_id;
    db_handler database;
    SQLiteDatabase sqLiteDatabase;
    Context context = this;
    Cursor cursor;
    RadioButton male , female , other;
    db_handler db;
    Integer i=0 , count ;

    Firebase root_ref_patient , root_ref_patient_mobile2key , root_ref_doctor2patient;
    private static final String REFERENCE_URL_DOCTOR2PATIENT = "https://chotahospital-ffc48.firebaseio.com/Doc2PatientMap";
    private static final String REFERENCE_URL_PATEIENT_MOBILE2KEY = "https://chotahospital-ffc48.firebaseio.com/PatientMobile2KeyMap";
    private static final String REFERENCE_URL_PATEIENT = "https://chotahospital-ffc48.firebaseio.com/Patients";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_patient);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("New Record");

        FirebaseDatabase database = FirebaseDatabaseUtil.getDatabase();

        Firebase.setAndroidContext(this);
        root_ref_patient = new Firebase(REFERENCE_URL_PATEIENT);
        root_ref_patient_mobile2key = new Firebase(REFERENCE_URL_PATEIENT_MOBILE2KEY);
        root_ref_doctor2patient = new Firebase(REFERENCE_URL_DOCTOR2PATIENT);

        contact_no = (EditText) findViewById(R.id.contact);
        name = (EditText) findViewById(R.id.name);
        height = (EditText) findViewById(R.id.pat_height);
        weight = (EditText) findViewById(R.id.pat_weight);
        age = (EditText) findViewById(R.id.age);
        male = (RadioButton) findViewById(R.id.male);
        female = (RadioButton) findViewById(R.id.female);
        other = (RadioButton) findViewById(R.id.other);

        i = getIntent().getIntExtra("Update",0);
        if(i == 1){
            setTitle("Update Record");
            // Toast.makeText(add_patient.this , "Entered update function ",Toast.LENGTH_SHORT).show();
            DatabaseReference patientinfo = database.getReference("Patients/" + getIntent().getStringExtra("patid"));
//            Firebase patientinfo = root_ref_patient.child(getIntent().getStringExtra("patid"));

            patientinfo.addValueEventListener(new com.google.firebase.database.ValueEventListener() {
                @Override
                public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {

                    pat_info patinfo = dataSnapshot.getValue(pat_info.class);

                    contact_no.setText(patinfo.getMobile().getNumber());
                    name.setText(patinfo.getName());
                    //email_id.setText(cursor.getString(2));
                    age.setText(patinfo.getAge());
                    height.setText(patinfo.getHeight());
                    weight.setText(patinfo.getWeight());
                    String gender = patinfo.getSex();

                    if(gender.equals("M")){
                        male.setChecked(true);
                    }
                    else if(gender.equals("F")){
                        female.setChecked(true);
                    }
                    else if(gender.equals("O")){
                        other.setChecked(true);
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }


    }

    public void add(View view){

        String contact = contact_no.getText().toString();
        String pat_name = name.getText().toString();
        String pat_height = height.getText().toString();
        String pat_weight = weight.getText().toString();
        String pat_age = age.getText().toString();


        if(male.isChecked()){
            gender = "M";
        }
        else if(female.isChecked()){
            gender = "F";
        }
        else{
            gender = "O";
        }

        pat_info patinformation = new pat_info();
        mobile mobile = new mobile();

        mobile.number = contact;
        mobile.countryCode = "91";

        patinformation.name = pat_name;
        patinformation.age = pat_age;
        patinformation.sex = gender;
        patinformation.mobile = mobile;
        patinformation.creationDate = "";
        patinformation.subscriptionExpiry = "";
        patinformation.height = pat_height;
        patinformation.weight = pat_weight;

        String errormsg = "";
        if(contact.equals("")){
            errormsg = errormsg + "Mobile Number field is empty ";
            //Toast.makeText(this , "Mobile Number field is empty" , Toast.LENGTH_LONG).show();
        }
        if(contact.length() != 10){

            errormsg = errormsg+"\n" + "Mobile Number incorrect ";
            //Toast.makeText(this , "Mobile Number incorrect" , Toast.LENGTH_LONG).show();
        }
        if(pat_name.equals("")){
            errormsg = errormsg +"\n" + "Name field is empty ";
            //Toast.makeText(this , "Enter your name" , Toast.LENGTH_LONG).show();
        }
        if(pat_height.equals("")){
            errormsg = errormsg +"\n" + "Height not entered ";
            //Toast.makeText(this , "Enter a valid email address",Toast.LENGTH_LONG).show();
        }
        if(pat_weight.equals("")){
            errormsg = errormsg +"\n" + "Weight not entered ";
            //Toast.makeText(this , "Enter a valid email address",Toast.LENGTH_LONG).show();
        }
        if(pat_age.equals("")){
            errormsg = errormsg +"\n" + "Age field is empty ";
            //Toast.makeText(this , "Enter your age" , Toast.LENGTH_LONG).show();
        }
        if(pat_age.equals("0")){
            errormsg = errormsg +"\n" + "Invalid age ";
            //Toast.makeText(this , "Enter a valid age" , Toast.LENGTH_LONG).show();
        }
        if(gender.equals("")){
            errormsg = errormsg +"\n " + "Gender not selected";
            //Toast.makeText(this , "Fill the gender field" , Toast.LENGTH_LONG).show();
        }

        if(!errormsg.equals("")){
            Toast.makeText(this , errormsg , Toast.LENGTH_LONG).show();
        }
        else if(i==1){
            root_ref_patient.child(getIntent().getStringExtra("patid")).setValue(patinformation);
            Toast.makeText(this , "New Patient added" , Toast.LENGTH_LONG).show();
            Intent i = new Intent(this , MainActivity.class);
            startActivity(i);
            this.finish();
        }
        else{

                final Firebase pushPateint = root_ref_patient.push();
                pushPateint.setValue(patinformation);

                Firebase PatientMobile2KeyMap = root_ref_patient_mobile2key.child("91" + contact);
                PatientMobile2KeyMap.push().setValue(pushPateint.getKey());

                Firebase doc2pat = root_ref_doctor2patient.child(getIntent().getStringExtra("docId"));
                doc2pat.child(pushPateint.getKey()).setValue(pushPateint.getKey());

                Toast.makeText(this , "New Patient added" , Toast.LENGTH_LONG).show();
                Intent i = new Intent(this , MainActivity.class);
                startActivity(i);
                this.finish();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_insulin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        /** if (id == R.id.editprofile) {
         Toast.makeText(this , "About to call Profile.class" , Toast.LENGTH_LONG).show();
         Intent intent = new Intent(add_insulin.this, Profile.class);
         intent.putExtra("Profile Update", 1);
         startActivity(intent);
         this.finish();
         }
         */

        if(id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            this.finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();

    }


}

class pat_info{

    String age , height , weight , creationDate , subscriptionExpiry , name , sex;
    mobile mobile;

    public String getAge() {
        return age;
    }

    public String getHeight() {
        return height;
    }

    public String  getWeight() {
        return weight;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getSubscriptionExpiry() {
        return subscriptionExpiry;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public mobile getMobile() {
        return mobile;
    }

    public pat_info() {
    }
}

class mobile{
    public String getNumber() {
        return number;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public mobile() {
    }

    String countryCode;
    String number;

}