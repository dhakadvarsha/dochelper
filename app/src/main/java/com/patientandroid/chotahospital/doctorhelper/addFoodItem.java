package com.patientandroid.chotahospital.doctorhelper;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class addFoodItem extends AppCompatActivity {

    Integer update = 0 , dataSelectedFromautoComplete = 0;
    String updateId,quantityPassUpdate;

    AutoCompleteTextView autoCompleteTextView;

    List<String> foodItem = new ArrayList<>();//list containing all the food item name from convertcsv.json
    List<String> quantity = new ArrayList<>();//list containing quantity corressponding to same index to foodItem list

    Map<String,String> map = new HashMap<>();
    Map<String,String> mapCalories = new HashMap<>();

    List<String> idList = new ArrayList<>();
    List<String> idName = new ArrayList<>();
    List<String> quantityTaken = new ArrayList<>();
    List<String> foodItemName = new ArrayList<>();

    AdapterFoodList adapterFoodList;
    Adapter_addFoodItem adapter_addFoodItem;

    Firebase root_ref_diet;
    private static final String REFERENCE_URL_DIET = "https://chotahospital-ffc48.firebaseio.com/Diet";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_breakfast);

        FirebaseDatabase database = FirebaseDatabaseUtil.getDatabase();
        Firebase.setAndroidContext(this);//Initialises firebase for using this android app
        root_ref_diet = new Firebase(REFERENCE_URL_DIET);//Reference to data into firebase cloud.

        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autocompleteBreakfast);
        autoCompleteTextView.setThreshold(1);
        adapterFoodList = new AdapterFoodList(getApplicationContext() , R.layout.autocompleterow);

        /**
         * loads the json file from assets to json array and then storing each foodItem
         * with it's quantity in lists by calling loadJSONFromAsset function.
         */
        try {
            JSONArray array = new JSONArray(loadJSONFromAsset());
            for(int i = 0;i<array.length();++i){
                foodItem.add(array.getJSONObject(i).get("FIELD2").toString());//adding data to foodItem list
                quantity.add(array.getJSONObject(i).get("FIELD5").toString());//adding data to quantity list

                FoodItem foodItem = new FoodItem();
                foodItem.foodItem = array.getJSONObject(i).get("FIELD2").toString();
                foodItem.quantity = array.getJSONObject(i).get("FIELD5").toString();

                map.put(array.getJSONObject(i).get("FIELD2").toString(),array.getJSONObject(i).get("FIELD1").toString());
                //mapping food items name with it's id
                mapCalories.put(array.getJSONObject(i).get("FIELD2").toString(),array.getJSONObject(i).get("FIELD4").toString());
                //mapping food item's name with it's calories per hundred gram.

                adapterFoodList.add(foodItem);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //sets adapter for autocompletetext view and passes the food item list to it.
        ArrayAdapter<String> aAdapter = new ArrayAdapter<>(this,android.R.layout.simple_dropdown_item_1line, foodItem);
        autoCompleteTextView.setAdapter(aAdapter);

        final ListView listView = (ListView) findViewById(R.id.list);

        /**
         * for loading the history of the person's diet (what is consumer on a given date.
         */
        DatabaseReference diet = database.getReference("Diet/" + getIntent().getStringExtra("patid")+"/"+
                getIntent().getStringExtra("Date")+"/"+getIntent().getStringExtra("AddTo"));
        diet.addValueEventListener(new com.google.firebase.database.ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {

                adapter_addFoodItem = new Adapter_addFoodItem(getApplicationContext(), R.layout.foodlist_row);
                listView.setAdapter(adapter_addFoodItem);

                idName.clear();
                idList.clear();
                foodItemName.clear();
                quantityTaken.clear();

                if(dataSnapshot.exists()){

                    for (com.google.firebase.database.DataSnapshot Reading : dataSnapshot.getChildren()) {
                        Log.e("Reading is",Reading.getKey());

                        storeInfo storeinfo = Reading.getValue(storeInfo.class);
                        idName.add(Reading.getKey());//adding keyName to idName.
                        String id = storeinfo.getItemId();
                        idList.add(id);//adding food item's if to idList
                        try {

                            JSONObject jsonObject = getJsonObject(id);

                            Food food= new Food();
                            foodItemName.add(jsonObject.getString("FIELD2"));//adding food item's name to foodItemName
                            food.fooditem = jsonObject.getString("FIELD2");
                            food.foodquantity = storeinfo.getQuantity();

                            quantityTaken.add(storeinfo.getQuantity());//adding quantity sto quantityTaken

                            if(!(jsonObject.getString("FIELD8").equals("-"))){
                                food.carbohydrates = "carbohydrates : "+jsonObject.getString("FIELD8")+" gm";
                            }else{
                                food.carbohydrates = "carbohydrates : "+jsonObject.getString("FIELD8");
                            }
                            if(!(jsonObject.getString("FIELD10").equals("-"))){
                                food.protein = "protein : "+jsonObject.getString("FIELD10")+" gm";
                            }else{
                                food.protein = "protein : "+jsonObject.getString("FIELD10");
                            }
                            /**Provide each of these to adapter for dispalying*/
                            adapter_addFoodItem.add(food);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        /**
         * Setting onItem click listener to ensure that the data has been selected only from the drop down list and
         *  then sets dataSelectedFromautoComplete as 1.
         */
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dataSelectedFromautoComplete = 1;
            }
        });

        /**
         * select an item from the list of already consumed food item inorder to update the information
         * previously entered.
         * get food item id from idList using the position , using food id good food items name from json file and finally
         * using the hashmaps got the id which is to be updated in the database and the previously consumed quantity
         * will autofill the quantity edittext.
         */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {

                    JSONObject jsonObject = getJsonObject(idList.get(position));
                    String foodname = jsonObject.getString("FIELD2");
                    autoCompleteTextView.setText(foodname);
                    update = 1;
                    updateId = idName.get(position);
                    quantityPassUpdate = quantityTaken.get(position);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dataSelectedFromautoComplete = 1;

            }
        });

        /**
         * long click the item to delete it fron the food list.
         * using the position , retrieve the food id using idlist and the get the food name from jsonfile and finally got the food
         * item's id that is to be deleted.
         * Alert box is fired , confirming the delete action.
         */

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {

                AlertDialog.Builder builder = new AlertDialog.Builder(addFoodItem.this);
                builder.setCancelable(true);
                builder.setTitle("Do you want to delete "+foodItemName.get(position)+"?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        String removeId = idName.get(position);
                        Firebase patient = root_ref_diet.child(getIntent().getStringExtra("patid"));
                        Firebase Date = patient.child(getIntent().getStringExtra("Date"));
                        Firebase breakfast = Date.child(getIntent().getStringExtra("AddTo"));

                        breakfast.child(removeId).removeValue();
                        autoCompleteTextView.setText("");
                    }

                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                    }

                });
                AlertDialog alert = builder.create();
                alert.show();

                return false;
            }
        });

    }

    /**
     * clear the autocomplete text field
     * @param v
     */
    public void clear(View v){

        autoCompleteTextView.setText("");
        update = 0;
    }

    /**
     * reads the json file form assets
     * @return string with json format
     */
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("convertcsv.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /**
     * returen jsonobject for particular id from json array
     * @param id
     * @return jsonobject with FIELD1 = id
     * @throws JSONException
     */
    public JSONObject getJsonObject(String id) throws JSONException {

        JSONArray jsonArray = new JSONArray(loadJSONFromAsset());
        Integer arrayPosition = Integer.parseInt(id);
        JSONObject jsonObject = jsonArray.getJSONObject(arrayPosition - 1);

        return jsonObject;
    }

    /**
     * called when the save button is clicked and quantity.class activity gets pop up.
     * @param view
     */
    public void Add(View view){

        if(dataSelectedFromautoComplete == 0){
            Toast.makeText(this , "Nothing selected from list" , Toast.LENGTH_LONG).show();
        }
        else{

            String foodId = map.get(autoCompleteTextView.getText().toString());

            Intent intent = new Intent(this,quantity.class);

            intent.putExtra("FoodItem",autoCompleteTextView.getText().toString());
            intent.putExtra("patid",getIntent().getStringExtra("patid"));
            intent.putExtra("Patient Name",getIntent().getStringExtra("Patient Name"));
            intent.putExtra("Date",getIntent().getStringExtra("Date"));
            intent.putExtra("foodid",foodId);
            intent.putExtra("update",update);
            intent.putExtra("AddTo",getIntent().getStringExtra("AddTo"));
            intent.putExtra("updateid",updateId);
            intent.putExtra("quantityPassUpdate",quantityPassUpdate);
            intent.putExtra("CaloriesPerGram",mapCalories.get(autoCompleteTextView.getText().toString()));

            startActivity(intent);
        }

    }

    /**
     * on back button pressed , view_diet activity is called
     */
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this, view_diet.class);
        intent.putExtra("patid",getIntent().getStringExtra("patid"));
        intent.putExtra("Patient Name",getIntent().getStringExtra("Patient Name"));
        intent.putExtra("Date",getIntent().getStringExtra("Date"));
        startActivity(intent);
        finish();

    }
}

/**
 * custom adapter for displaying the list of history contents
 */

class Adapter_addFoodItem extends ArrayAdapter {
    List list = new ArrayList();

    public Adapter_addFoodItem(Context context, int resource) {
        super(context, resource);
    }

    static  class LayoutHandler{
        TextView fooditem , foodquantity , carbohydrates , protein;
    }

    public void add(Object object) {
        super.add(object);
        /**Adding each of the dataprovider content in list*/
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    /**Will return each row of data*/
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        /**Check if already the row is available or not*/
        LayoutHandler layoutHandler;
        if(row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.foodlist_row , parent , false);
            layoutHandler = new LayoutHandler();
            layoutHandler.fooditem = (TextView) row.findViewById(R.id.foodName);
            layoutHandler.foodquantity = (TextView) row.findViewById(R.id.foodQuantity);
            layoutHandler.carbohydrates = (TextView) row.findViewById(R.id.carbohydrates);
            layoutHandler.protein = (TextView) row.findViewById(R.id.protein);
            row.setTag(layoutHandler);
        }
        else{
            layoutHandler = (LayoutHandler) row.getTag();
        }

        Food food = (Food) this.getItem(position);
        layoutHandler.fooditem.setText(food.fooditem);
        layoutHandler.foodquantity.setText(food.foodquantity);
        layoutHandler.carbohydrates.setText(food.carbohydrates);
        layoutHandler.protein.setText(food.protein);
        return row;
    }

}

/**
 * class to store objects for array adapter
 */
class Food{
    public String fooditem , foodquantity , carbohydrates , protein;

}


/**
 * class to store objects as stored in the database
 */
 class storeInfo{

     public String itemId;
     public String quantity;
     public String caloriesConsumed;

     public storeInfo() {

     }

     public String getCaloriesConsumed() {
         return caloriesConsumed;
     }

     public String getItemId() {
         return itemId;
     }

     public String getQuantity() {
         return quantity;
     }

     public storeInfo(String itemId, String quantity,String caloriesConsumed) {

         this.itemId = itemId;
         this.quantity = quantity;
         this.caloriesConsumed = caloriesConsumed;
     }
 }
