package com.patientandroid.chotahospital.doctorhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by varsh on 03-06-2016.
 */
public class Adapter_insulin extends ArrayAdapter {

    List list = new ArrayList();

    public Adapter_insulin(Context context, int resource) {
        super(context, resource);
    }

    static  class LayoutHandler{
        TextView intake , start , end , times , type ,reminder;
    }

    public void add(Object object) {
        super.add(object);
        /**Adding each of the dataprovider content in list*/
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    /**Will return each row of data*/
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        /**Check if already the row is available or not*/
        LayoutHandler layoutHandler;
        if(row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.insulin_row , parent , false);
            layoutHandler = new LayoutHandler();
            layoutHandler.intake = (TextView) row.findViewById(R.id.row1);
            layoutHandler.start = (TextView) row.findViewById(R.id.row2);
            layoutHandler.end = (TextView) row.findViewById(R.id.row2a);
            layoutHandler.times = (TextView) row.findViewById(R.id.row3);
            layoutHandler.type = (TextView) row.findViewById(R.id.row4);
            layoutHandler.reminder = (TextView) row.findViewById(R.id.row4b);
            row.setTag(layoutHandler);
        }
        else{
            layoutHandler = (LayoutHandler) row.getTag();
        }
        Insulin insulin = (Insulin) this.getItem(position);
        layoutHandler.intake.setText(insulin.i_intake_value);
        layoutHandler.start.setText(insulin.i_s_date);
        layoutHandler.end.setText(insulin.i_e_date);
        layoutHandler.type.setText(insulin.i_type);
        if(insulin.i_reminder == 1){
            layoutHandler.reminder.setText("Reminder : ON");
        }
        else{
            layoutHandler.reminder.setText("Reminder : OFF");
        }
        Integer x = insulin.i_breakfast + insulin.i_lunch + insulin.i_snacks + insulin.i_dinner + insulin.i_night;
        layoutHandler.times.setText(x.toString());
        return row;
    }
}