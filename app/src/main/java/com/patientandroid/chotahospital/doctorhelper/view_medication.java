package com.patientandroid.chotahospital.doctorhelper;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class view_medication extends AppCompatActivity {

    db_handler database;
    Context context = this;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;
    Intent i;
    Medication single_data;
    String removeFromid;

    Map<Integer,String> mapPositionToid = new HashMap<Integer, String>();
//    List<Map<String,String>> idList = new ArrayList<Map<String, String>>();

//    List<String> idDate = new ArrayList<>();

//    String OnDate , Onid;

    Firebase root_ref;
    private static final String REFERENCE_URL = "https://chotahospital-ffc48.firebaseio.com/Medication";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_medication);

        final FirebaseDatabase database = FirebaseDatabaseUtil.getDatabase();
        Firebase.setAndroidContext(this);//Initialises firebase for using this android app
        root_ref = new Firebase(REFERENCE_URL);//Reference to data into firebase cloud.

        final ListView med_list=(ListView)findViewById(R.id.med_list);
        registerForContextMenu(med_list);


//        final Firebase contact = root_ref.child(getIntent().getStringExtra("Contact_No"));
//        Firebase patient = contact.child("Medication");

        DatabaseReference patient = database.getReference("Medication/" + getIntent().getStringExtra("patid"));
        patient.addValueEventListener(new com.google.firebase.database.ValueEventListener() {
                  @Override
                  public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                      final ArrayList<Medication> arr = new ArrayList<>();
                      int atPosition = 0;
                          for (com.google.firebase.database.DataSnapshot Reading : dataSnapshot.getChildren()) {
                              Log.e("Reading is", Reading.getKey());
                              mapPositionToid.put(atPosition , Reading.getKey());
//                              idList.add(map);
                              ++atPosition;
//                              idDate.add(Reading.getKey());
                              Medication MedicineReading = Reading.getValue(Medication.class);

                              Medication temp = new Medication();
                              temp.m_name = MedicineReading.getM_name();
                              temp.m_quantity = MedicineReading.getM_quantity();
                              temp.m_s_date = MedicineReading.getM_s_date();
                              temp.m_e_date = MedicineReading.getM_e_date();
                              temp.m_time = MedicineReading.getM_time();
                              arr.add(temp);

                          }
                          Log.e("arr size",String.valueOf(arr.size()));
                          Medication[] object_data = new Medication[arr.size()];
                          int i1=0;
                          while(i1<arr.size()) {
                              object_data[i1]=arr.get(i1);
                              i1++;
                          }
                          ListAdapter la = new customAdapter1(context,object_data);
                          med_list.setAdapter(la);

                      }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        med_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                single_data = (Medication) parent.getItemAtPosition(position);

                removeFromid = mapPositionToid.get(position);
//                OnDate = removeFrom.get(idDate.get(position));
//                Onid = idDate.get(position);

                Log.e("UpdateAtid is",removeFromid);
                i = new Intent(view_medication.this, add_med.class);
                i.putExtra("patid", getIntent().getStringExtra("patid"));
                i.putExtra("UpdateAtid",removeFromid);

                return false;
            }
        });
    }
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add("Edit Medicine");
        menu.add("Delete Medicine");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);

        if(item.getTitle()=="Edit Medicine")
        {
            i.putExtra("flag", "1");
            String ss=getIntent().getStringExtra("patid");
            i.putExtra("patid", ss);
            i.putExtra("DateUpdate",1);
            startActivity(i);
        }
        else if(item.getTitle()=="Delete Medicine"){
//            i = new Intent(this, view_medication.class);

            Firebase contact = root_ref.child(getIntent().getStringExtra("patid"));
//            Firebase onDate = patient.child(OnDate);
            contact.child(removeFromid).removeValue();

            Toast.makeText(view_medication.this,"Data Deleted", Toast.LENGTH_LONG).show();

//            i.putExtra("Contact_No", getIntent().getStringExtra("Contact_No"));
////            ob.delMed(single_data,single_data.m_name);
//            finish();
//
//            startActivity(i);
        }
        return true;
    }
    public void add(View v){
        Intent i=new Intent(this,add_med.class);
        //String ss=getIntent().getStringExtra("patid");
        Log.e("patid to add med ", getIntent().getStringExtra("patid"));
        i.putExtra("patid", getIntent().getStringExtra("patid"));
        i.putExtra("flag","0");
        startActivity(i);
    }

}

class customAdapter1 extends ArrayAdapter {

    customAdapter1(Context context, Medication[] data) {
        super(context, R.layout.med_rod,data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater buckysInflater = LayoutInflater.from(getContext());
        View customView =buckysInflater.inflate(R.layout.med_rod, parent,false);

        Medication single_data= (Medication) getItem(position);
        TextView med_name=(TextView) customView.findViewById(R.id.med_name);
        TextView med_quantity =(TextView) customView.findViewById(R.id.med_quantity);
        TextView med_end=(TextView) customView.findViewById(R.id.med_end_date);
        TextView med_time=(TextView) customView.findViewById(R.id.med_time);

        med_name.setText(single_data.m_name);
        med_quantity.setText(single_data.m_quantity);
        med_end.setText("Until: "+single_data.m_e_date);
        String test=single_data.m_time;
        int i=0,j=0;
        {
            while(j<test.length())
            {
                if(test.charAt(j)=='-'){
                    i++;
                }
                j++;
            }
        }
        String l=""+i;
        med_time.setText(l+" times a day");

        return customView;
    }
}
