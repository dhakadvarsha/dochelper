package com.patientandroid.chotahospital.doctorhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by varsh on 24-06-2016.
 */

/**
 * custom adapter to show the drop down list in autocompletextview
 */
public class AdapterFoodList extends ArrayAdapter {
    List list = new ArrayList();

    public AdapterFoodList(Context context, int resource) {
        super(context, resource);
    }

    static  class LayoutHandler{
        TextView foodItem , quantity;
    }

    public void add(Object object) {
        super.add(object);
        /**Adding each of the dataprovider content in list*/
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    /**Will return each row of data*/
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        /**Check if already the row is available or not*/
        LayoutHandler layoutHandler;
        if(row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.autocompleterow , parent , false);
            layoutHandler = new LayoutHandler();
            layoutHandler.foodItem = (TextView) row.findViewById(R.id.row1);
            layoutHandler.quantity = (TextView) row.findViewById(R.id.row1a);
            row.setTag(layoutHandler);
        }
        else{
            layoutHandler = (LayoutHandler) row.getTag();
        }

        FoodItem foodItem = (FoodItem) this.getItem(position);
        layoutHandler.foodItem.setText(foodItem.foodItem);
        layoutHandler.quantity.setText(foodItem.quantity);
        return row;
    }
}


class FoodItem {

    public String foodItem;
    public String quantity;

}
